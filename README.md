# How to use
1. Run the main file as a python module, this will initially create all the necessary files/folders to run the budget.
2. Download all documents you wish to analyze into raw_export_files.
3. Rerun main.py as a module and work your way through the tui.
4. Reports will be automatically generated in reports/{current_year} folder

## Notes
- This program automatically backs up the database before making changes, these backups can be found in program/sql/backups. The backups get overwritten every year.
- Program currently only works for my specifc file structures, I'll work on making it more generic later.


## TODO
- Make file importing more generic
    - When a unrecognized file structure is imported give the user the ability to define importing rules.
- Give user ability to change the folder location for import and report folder.
