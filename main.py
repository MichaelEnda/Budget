from program.utils.folder_gen import create_folders
from program.sql.importer import NewImporter
from program.sql.gen_tables import GenTables
from program.sql import HistorySQL, SpendingSQL
from program.tui.app import BudgetApp 
from program.utils import UseDates
from program.gen_reports.report_builder import GenerateReport
from program.sql.sql_access import SQL_access

class EoM(object):
    def setup(self):
        folder_list = [
                'reports/', 
                'program/sql/backup/', 
                'raw_export_files/'
                ]
        for fgen in folder_list:
            create_folders(fgen)
        SQL_access().backup()
        GenTables().main()
        NewImporter().main()

    def userinput(self) -> dict[str, float] | None:
        app = BudgetApp()
        sorted_spending = app.run()
        return sorted_spending

    def main(self):
        self.setup()
        self.userinput()
        GenerateReport().main() 


if __name__ == "__main__":
    EoM().main()
