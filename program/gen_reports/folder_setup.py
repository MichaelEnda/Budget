from datetime import datetime
import os

from ..utils import UseDates

class PDFUtils(object):
    def __init__(self):
        self.dates = UseDates()
        self.stdatetime = self.dates.stdatetime
        self.folder_setup()

    def _report_folder_setup(self) -> str:
        reports_path = os.path.join(os.getcwd(), "reports")
        if not os.path.isdir(reports_path):
            os.mkdir(reports_path)
        return reports_path

    def _year_folder_setup(self, reportpath: str):
        year_name = datetime.strftime(self.stdatetime, "%Y")
        year_folder = os.path.join(reportpath, year_name)
        if not os.path.isdir(year_folder):
            os.mkdir(year_folder)
        return year_folder

    def _month_folder_setup(self, yearpath: str):
        month_name = datetime.strftime(self.stdatetime, "%m-%b")
        month_folder = os.path.join(yearpath, month_name)
        if not os.path.isdir(month_folder):
            os.mkdir(month_folder)
        return month_folder

    def folder_setup(self):
        reports_path = self._report_folder_setup()
        year_path = self._year_folder_setup(reports_path)
        self.folder_path = year_path




if __name__ == "__main__":
    PDFUtils().folder_setup()
