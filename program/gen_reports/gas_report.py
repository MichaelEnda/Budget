from datetime import datetime, date
from reportlab.lib.pagesizes import inch
from reportlab.platypus import Paragraph
from reportlab.platypus.flowables import Spacer
from reportlab.platypus.tables import colors

from .paragraph_styles import ParagraphStyles
from .table_styles import TableStyles
from ..sql.gas_sql import GasSQL
from ..sql.maint_sql import MaintSQL
from ..sql.miles_sql import MilesSQL
from .utils import UseDates

class GasReport(object):
    def __init__(self, 
                 stdate: date = UseDates.stdate,
                 enddate: date = UseDates.enddate):
        miles_sql = MilesSQL()
        self.curmiles = miles_sql.get_miles(search=enddate)

        gas_sql = GasSQL()
        self.gas_avgs = gas_sql.get_averages()
        self.gas_actual = gas_sql.getrow(search=stdate)

        self.tstyles = TableStyles()

    def avg_table(self):
        first_col = ["Miles", "Gallons", "MPG"]
        header = ['', "avg", "actual"]
        data = [header]
        styles = [
                self.tstyles.grid(),
                self.tstyles.text_color(),
                self.tstyles.background()
                ]
        # Itterates through the rows for to potential grid
        for row, average in enumerate(self.gas_avgs, 1):
            actual = getattr(self.gas_actual, average.name)
            # Generates each data row as Name | average | value
            data.append([first_col[row-1], average.value, actual])
            # If the current value is greater than the average higlights that row
            if actual > average.value:
                styles.append(
                        self.tstyles.background(
                            start_cell= (1, row),
                            end_cell = (-1, row),
                            color = colors.red
                            )
                        )

                styles.append(self.tstyles.text_color(
                            start_cell = (row, 1),
                            end_cell = (row, -1),
                            color = colors.black
                            )
                        )
        table = self.tstyles.gen_table(data= data, styles= styles)
        return table

    def gen_gas_report(self):
       car_write = [] 
       car_write.append(ParagraphStyles().header("Gas"))
       car_write.append(self.avg_table())
       return car_write
