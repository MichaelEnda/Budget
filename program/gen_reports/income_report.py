from reportlab.lib.pagesizes import inch
from reportlab.platypus import Image, Paragraph
from reportlab.platypus.flowables import Spacer

from .paragraph_styles import ParagraphStyles
from .table_styles import TableStyles
from ..sql import HistorySQL
from .utils import UseDates
from ..graphers.pie_chart import PieChart


class IncomeReports:
    def __init__(self):
        self.hist_data = HistorySQL().getrow(UseDates().stdatestr)
        self.p = ParagraphStyles()
        self.t = TableStyles()

    def income_header(self):
        return self.p.header(text="Income")

    def income_content(self):
        income_include = [
                    "wages",
                    "scholarships",
                    "otherinc",
                    "netinc"
                    ]
        return self.p.bullet_points(
                attrs=income_include,
                d=self.hist_data,
                include=True
                )

    def income_graph(self) -> Image:
        graph_include = [
                    "wages",
                    "scholarships",
                    "otherinc",
                    ]
        image_loc = PieChart().pie_chart(
                title='Income',
                attrs=graph_include,
                include=True,
                d=[self.hist_data]
                )
        draw_image = Image(image_loc, width=400, height=300)
        return draw_image

    def gen_income(self):
        income_write = []
        income_write.append(self.income_header())
        income_write.append(Spacer(1, 0.015*inch))
        income_table_data = [
                [self.income_content(), self.income_graph()]
                ]
        income_table = self.t.gen_table(data=income_table_data, styles=self.t.text_to_left_of_graph, colWidths=[150, None])
        income_write.append(income_table)
        return income_write
