from dataclasses import astuple
from datetime import datetime, date
from reportlab.lib.pagesizes import inch
from reportlab.platypus import Paragraph
from reportlab.platypus.flowables import Spacer
from reportlab.platypus.tables import Table, colors

from .paragraph_styles import ParagraphStyles
from .table_styles import TableStyles
from ..sql.maint_check_sql import MaintCheckSQL
from ..sql.miles_sql import MilesSQL


class MaintReport(object):
    def __init__(self) -> None:
        self.report_data = MaintCheckSQL().report_data()
        self.curmiles = MilesSQL().get_miles()
        self.pstyles = ParagraphStyles()
        self.tstyles = TableStyles()


    def highlight_cells(self, report_row: MaintCheckSQL.ReportData, 
                            style_list: list,
                            table_row: int):
        if report_row.check_date():
            style_list.append(self.tstyles.background(start_cell=(2, table_row),
                                                end_cell=(2, table_row),
                                                color=colors.red)
                              )
        if report_row.check_miles(self.curmiles):
            style_list.append(self.tstyles.background(start_cell=(4, table_row),
                                                end_cell=(4, table_row),
                                                color=colors.red)
                              )

        return style_list

    def maint_table(self) -> Table:
        header = ("", "last date", "next date", "last miles", "next miles")
        table_data = [header]
        table_style = [
                *self.tstyles.default_styles,
                ]
        for table_row, report_row in enumerate(self.report_data, 1):
            table_data.append(astuple(report_row))
            table_style = self.highlight_cells(report_row, table_style, table_row)
        return self.tstyles.gen_table(data=table_data, styles=table_style)


    def gen_maint_story(self):
        return [
                self.pstyles.header("Maintenance"), 
                self.pstyles.subheader(f"Current Mileage: {self.curmiles}"),
                self.maint_table()
                ]
