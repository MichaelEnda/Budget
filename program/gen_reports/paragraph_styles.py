from typing import Iterable
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Flowable, ListFlowable, ListItem, Paragraph
from .utils import Reader, ReportData

class ParagraphStyles(object):
    def __init__(self):
        self.styles = getSampleStyleSheet()

    def header(self, text: str) -> Paragraph:
        return Paragraph(
                text,
                style=self.styles['h2']
                )

    def subheader(self, text:str) -> Paragraph:
        return Paragraph(
                text,
                style=self.styles['h5']
                )

    def bullet_points(self, attrs:list[str], d:Reader, include:bool=True) -> Flowable:
        if include:
            data = ReportData(get_from=d, include=attrs).for_bullets
        else:
            data = ReportData(get_from=d, exclude=attrs).for_bullets
        points = []
        for bullet in data:
            points.append(
                    ListItem(
                        Paragraph(
                            bullet, self.styles['Normal']
                            ),
                        bulletColor="black",
                        value='*'
                        )
                    )
        return ListFlowable(points, bulletType='bullet')



