from calendar import monthrange
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
from reportlab.platypus import Flowable, Image, Paragraph, Table

from .table_styles import TableStyles
from .paragraph_styles import ParagraphStyles
from ..graphers.line_chart import LineChart
from ..utils import UseDates
from ..sql import HistorySQL, OneTimeSQL
from ..sql.temp_recurring import TempRecurring

class PlanningReport(object):
    def __init__(self) -> None:
        hist_sql = HistorySQL()
        self.avg_inc, self.avg_spend = hist_sql.avgs
        self.savings = hist_sql.savings
        self.saving_goal = hist_sql.saving_goal

        self.onetime_sql = OneTimeSQL()


        self.pstyles = ParagraphStyles()
        self.tstyles = TableStyles()

        self.stdate = UseDates().stdate
        
        self.goal_date = None
        self.bankrupt_date = "N/A"
        self.grapher_dates, self.grapher_values, self.recurring_vlines = self._calculate_future(self.avg_inc)

        self.chart_builder = LineChart()
        self._gen_base_graph()
        self._add_savings_cutoff()

    @property
    def planning_header(self) -> Paragraph:
       return self.pstyles.header("Planning") 

    @property
    def current_savings_sub_header(self) -> Paragraph:
        return self.pstyles.subheader(f"Current Savings: {self.savings}")

    @property
    def savings_goal_subheader(self) -> Paragraph:
        return self.pstyles.subheader(f"Savings goal: {self.saving_goal}")

    @property
    def date_of_savings_goal(self) -> Paragraph:
        return self.pstyles.subheader(f"Date savings goal is reached: {self.goal_date}")

    @property
    def expected_bankruptcy(self) -> Paragraph:
        return self.pstyles.subheader(f"Date of expected bankruptcy: {self.bankrupt_date}")

    def _daily_expend(self, year: int, month: int) -> float:
        days = monthrange(year=year, month=month)[1]
        return self.avg_spend / days

    def _daily_income(self, year: int, month: int, monthly_inc: float) -> float:
        days = monthrange(year=year, month=month)[1]
        return monthly_inc / days

    def _daily_change(self, year: int, month:int, monthly_inc: float) -> float:
        return self._daily_income(year, month, monthly_inc) + self._daily_expend(year, month)

    def _calculate_future(self, monthly_inc: float) -> tuple[list[date], list[float], dict[str, list[date]]]:
        temp_recur = TempRecurring()
        savings = self.savings
        curdate = self.stdate
        ret_dates = []
        ret_savings = []
        vert_lines = {}
        while savings > 0 and curdate.year <= (self.stdate + relativedelta(years=+1)).year:
            curdate += timedelta(days=1)
            savings += self._daily_change(
                    year=curdate.year, 
                    month=curdate.month,
                    monthly_inc=monthly_inc
                    )
            # These are -= as the data in onetime/recurring are stored as positive numbers
            vert_line = temp_recur.update_date(curdate)
            for line in vert_line:
                cur = vert_lines.get(line[0], [])
                cur.append(line[1])
                vert_lines.update({line[0]: cur})
            recur_cost = temp_recur.get_cost_from_date(curdate)
            savings -= recur_cost
            onetime_cost = self.onetime_sql.get_cost_by_date(curdate)
            savings -= onetime_cost
            if savings <= self.saving_goal and self.goal_date is None:
                self.goal_date = curdate
            ret_savings.append(savings)
            ret_dates.append(curdate)
        if savings < 0:
            self.bankrupt_date = curdate

        self.grapher_dates = ret_dates
        self.grapher_values = ret_savings
        self.recurring_vlines = vert_lines
        return ret_dates, ret_savings, vert_lines

    def _gen_base_graph(self):
        self.chart_builder.set_labels(
                title="Default",
                xlabel="Dates",
                ylabel="Savings"
                )
        self.chart_builder.plot_main_line(
                x_data=self.grapher_dates,
                y_data=self.grapher_values
                )

    def _add_savings_cutoff(self):
        labels = ["Savings Goal"]
        data = [[self.saving_goal]]
        self.chart_builder.base_plot = self.chart_builder.plot_secondary_lines(labels=labels, data=data, vert=False)

    def _gen_recurring_graph(self) -> str:
        labels = []
        data = []
        for label, datum in self.recurring_vlines.items():
            labels.append(label)
            data.append(datum)
        recurring_graph = self.chart_builder.plot_secondary_lines(labels=labels, data=data)
        recurring_graph.title = "Recurring Purchases"
        return self.chart_builder.save(chart=recurring_graph)

    def _gen_onetime_graph(self) -> str:
        ot_data = OneTimeSQL().gettable()
        labels = [row.goal for row in ot_data]
        data = [[row.date] for row in ot_data]
        onetime_graph = self.chart_builder.plot_secondary_lines(labels=labels, data=data)
        onetime_graph.title = "Onetime Purchases"
        return self.chart_builder.save(chart=onetime_graph)

    @property
    def one_time_and_recurring_graphs(self) -> Table:
        onetime_image = Image(self._gen_onetime_graph(), width=400, height=300)
        recurring_image = Image(self._gen_recurring_graph(), width=400, height=300)
        table_data = [[onetime_image], [recurring_image]]
        table = self.tstyles.gen_table(data=table_data, 
                                       styles=[
                                        ('ALIGN', (0,0), (-1,-1), "CENTER"),
                                        ('VALIGN', (0,0), (-1,-1), "TOP")
                                           ])
        return table

    @property
    def planning_report(self) -> list[Flowable]:
        planning_report = [
                self.planning_header,
                self.current_savings_sub_header,
                self.savings_goal_subheader,
                self.date_of_savings_goal,
                self.expected_bankruptcy,
                self.one_time_and_recurring_graphs
                ]
        return planning_report




if __name__ == "__main__":
    PlanningReport()._gen_recurring_graph()

    PlanningReport()._gen_onetime_graph()
