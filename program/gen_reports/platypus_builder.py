from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib import colors
from reportlab.lib.pagesizes import inch, letter
from datetime import date, datetime, timedelta
import os
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus.tables import Table
from reportlab.rl_config import defaultPageSize

from .utils import UseDates
from .folder_setup import PDFUtils

class ReportBuilder(object):
    def __init__(self) -> None:
        self.page_height = defaultPageSize[1]
        self.page_width = defaultPageSize[0]
        self.styles = getSampleStyleSheet()
        self._story = []
        self._story.append(Spacer(1, 0.015*inch))
        self.canvas = canvas 
        date = UseDates().stdatetime
        self.title = datetime.strftime(date, "%m-%b")
        pdfmetrics.registerFont(
            TTFont('tnr', 'times-new-roman.ttf')
        )

        
    @property
    def story(self):
        return self._story

    @story.setter
    def story(self, paragraph: Paragraph | Spacer):
        # Adds the list of strings which constitute a Paragraph (built elsewhere)
        self._story.append(paragraph)

    def first_page(self, canvas: canvas.Canvas, doc):
        canvas.saveState()
        canvas.setFont('tnr', 36)
        canvas.drawCentredString(self.page_width/2,
                                  self.page_height-80,
                                  self.title)
        canvas.line(0, self.page_height-100, 700, self.page_height-100)
        canvas.restoreState()

    def later_page(self, canvas: canvas.Canvas, doc):
        canvas.saveState()
        canvas.setFont('tnr', 12)
        canvas.restoreState()

    def builder(self):
        # calling PDFUtils.folder_location should create the necessary file structure
        file_path = os.path.join(PDFUtils().folder_path, f"{self.title}.pdf")
        # These values are in points - 72 points = 1 inch
        side_margin = 36
        top_margin = 36
        doc = SimpleDocTemplate(file_path, pagesize=letter, leftMargin=side_margin, rightMargin=side_margin,
                                topMargin=top_margin, bottomMargin=top_margin)
        doc.build(self._story,
                  onFirstPage=self.first_page,
                  onLaterPages=self.later_page
                  )

if __name__ == "__main__":
   ReportBuilder().builder() 
