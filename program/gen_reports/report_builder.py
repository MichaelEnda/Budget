from reportlab.platypus import Flowable, Paragraph, Spacer, Table
from .platypus_builder import ReportBuilder
from .spending_report import SpendingReport
from .income_report import IncomeReports
from .gas_report import GasReport
from .maintenance_report import MaintReport
from .planning_report import PlanningReport

class GenerateReport(ReportBuilder):
    def add_to_story(self, report: list):
        for f in report:
            self.story = f
    
    def main(self):
        self.add_to_story(SpendingReport().gen_spending())
        self.add_to_story(IncomeReports().gen_income())
        self.add_to_story(GasReport().gen_gas_report())
        self.add_to_story(MaintReport().gen_maint_story())
        self.add_to_story(PlanningReport().planning_report)
        self.builder()

if __name__ == "__main__":
    GenerateReport().main()

