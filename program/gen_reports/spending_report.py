from reportlab.lib.pagesizes import inch
from reportlab.platypus import Paragraph, Image
from reportlab.platypus.flowables import Spacer

from .paragraph_styles import ParagraphStyles
from .table_styles import TableStyles
from ..graphers.pie_chart import PieChart 
from ..sql import HistorySQL
from .utils import UseDates, ReportData


class SpendingReport(object):
    def __init__(self):
        self.hist_data = HistorySQL().getrow(UseDates().stdatestr)
        self.p = ParagraphStyles()
        self.t = TableStyles()

    def spending_header(self):
        return self.p.header(text="Expenditure")

    def spending_content(self):
        spending_include = [
                "groceries",
                "gas",
                "eattingout",
                "vices",
                "rent",
                "bill",
                "investing",
                "otherexpend",
                'netexp'
                ]
        return self.p.bullet_points(
                d=self.hist_data,
                attrs=spending_include,
                include=True
                )

    def spending_graph(self):
        include = ['groceries', 'gas', 'eattingout', 'vices', 'rent', 'bill', 'investing', 'otherexpend']
        img_loc = PieChart().pie_chart(
                title='Expenditure', 
                attrs=include, 
                include=True, 
                d=[self.hist_data]
                )
        graph_img = Image(img_loc, width=400, height=300)
        graph_img.hAlign = 'LEFT'
        return graph_img

    def gen_spending(self) -> list[Paragraph | Spacer]:
        spending_write = []
        spending_write.append(self.spending_header())
        spending_write.append(Spacer(1, 0.015*inch))
        spending_table = [
                [self.spending_content(), self.spending_graph()]
                ]
        spend_styles = [*self.t.text_to_left_of_graph]
        # The colwidths variables need to be in points, for some reason doesn't work with percentages
        content = self.t.gen_table(data=spending_table, styles=spend_styles, colWidths=[150, None])
        spending_write.append(content)
        return spending_write

if __name__ == "__main__":
    pass
