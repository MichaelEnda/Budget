from dataclasses import dataclass, field
from operator import methodcaller
from typing import Literal
from reportlab.platypus import  Table, TableStyle
from reportlab.lib import colors



class TableStyles(object):
    @dataclass
    class _grid_index:
        _x: int
        _y: int
        index: tuple = field(init=False)

        def __post_init__(self):
            self.index = (self._x, self._y)

    @dataclass
    class line:
        line_type: Literal["GRID", "BOX", "LINEABOVE", "LINEBELOW", "LINEBEFORE", "LINEAFTER"]
        start: tuple
        end: tuple
        color: colors.Color 
        width: float = field(default=1)

        @property
        def command(self):
            return [self.line_type, self.start, self.end, self.width, self.color]

    @dataclass
    class cell_color:
        format_type: Literal[ 
                             "TEXTCOLOR",
                             "BACKGROUND",
                             ]
        start: tuple
        end: tuple
        color: colors.Color

        @property
        def command(self):
            return [self.format_type, self.start, self.end, self.color]

    @property
    def default_styles(self):
        return [self.grid(), self.background(), self.text_color()]

    @property
    def text_to_left_of_graph(self):
        style_list = [
                # Alighn all cells to the left
                ('ALIGN', (0,0), (-1,-1), "LEFT"),
                # align all cells to top
                ('VALIGN', (0,0), (-1,-1), "TOP")
                ]
        return style_list

    def grid(self, end_cell: tuple[int, int] = (-1, -1), 
             start_cell: tuple[int, int] = (0, 0),
             color: colors.Color = colors.black) -> list:
        return_grid = self.line(
                line_type="GRID",
                start= start_cell,
                end= end_cell,
                color = color
                )
        return return_grid.command

    def background(self, end_cell: tuple[int, int] = (-1, -1),
                   start_cell: tuple[int, int] = (0,0),
                   color: colors.Color = colors.white) -> list:
        return_background = self.cell_color(
                    format_type="BACKGROUND",
                    start= start_cell,
                    end= end_cell,
                    color = color
                )
        return return_background.command

    def text_color(self, end_cell: tuple[int, int] = (-1, -1),
                   start_cell: tuple[int, int] = (0,0),
                   color: colors.Color = colors.black) -> list:
        return_text_color = self.cell_color(
                format_type="TEXTCOLOR",
                start= start_cell,
                end = end_cell,
                color = color
                )
        return return_text_color.command


    def gen_table(self, data: list, styles:list, **kwargs) -> Table:
        return Table(data, style=styles, **kwargs)




