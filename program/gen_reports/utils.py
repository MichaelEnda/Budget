from dataclasses import dataclass, field, fields
from datetime import datetime
import os

from ..utils import UseDates
from ..sql.sql_utils import Reader


@dataclass
class ReportData:
    get_from: Reader
    include: list[str] = field(default_factory=list[str])
    exclude: list[str] = field(default_factory=list[str])
    _data: list[tuple] = field(default_factory=list, init=False)

    def __post_init__(self):
        if self.include and self.exclude:
            raise ValueError("Cannot define include and exclude simultaniously")
        self._get()

    def _search_parent_include(self):
        for field in fields(self.get_from):
            if field.name in self.include:
                field_value = getattr(self.get_from, field.name)
                self._data.append((field.name, field_value))

    def _search_parent_exclude(self):
        for field in fields(self.get_from):
            if field.name not in self.include:
                field_value = getattr(self.get_from, field.name)
                self._data.append((field.name, field_value))

    def _search_all_parent(self):
        for field in fields(self.get_from):
            field_value = getattr(self.get_from, field.name)
            self._data.append((field.name, field_value))

    def _get(self):
        if self.include and not self.exclude:
            self._search_parent_include()
        elif self.exclude and not self.include:
            self._search_parent_exclude()
        else:
            self._search_all_parent()

    @property
    def for_bullets(self):
        return [f"{x[0]}: {x[1]}" for x in self._data]



class PDFUtils(object):
    def __init__(self):
        self.dates = UseDates
        self.stdatetime = self.dates.stdatetime

    def _report_folder_setup(self) -> str:
        reports_path = os.path.join(os.getcwd(), "reports")
        if not os.path.isdir(reports_path):
            os.mkdir(reports_path)
        return reports_path

    def _year_folder_setup(self, reportpath: str):
        year_name = datetime.strftime(self.stdatetime, "%Y")
        year_folder = os.path.join(reportpath, year_name)
        if not os.path.isdir(year_folder):
            os.mkdir(year_folder)
        return year_folder

    def _month_folder_setup(self, yearpath: str):
        month_name = datetime.strftime(self.stdatetime, "%m-%b")
        month_folder = os.path.join(yearpath, month_name)
        if not os.path.isdir(month_folder):
            os.mkdir(month_folder)
        return month_folder

    def folder_setup(self):
        reports_path = self._report_folder_setup()
        year_path = self._year_folder_setup(reports_path)
        month_path = self._month_folder_setup(year_path)
