from dataclasses import dataclass, field, fields
from datetime import date
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from os import path, mkdir
from tempfile import gettempdir

from ..sql import HistorySQL, SpendingSQL, Reader
from ..utils import UseDates
from .utils import Grapher

@dataclass
class ChartData:
    get_from: Reader
    include: list[str] = field(default_factory=list[str])
    exclude: list[str] = field(default_factory=list[str])
    data: dict = field(default_factory=dict[str, float | int])

    def __post_init__(self):
        if self.include and self.exclude:
            raise ValueError("Cannot define include and exclude simultaniously")
        self._get()

    def _update_data(self, key:str, value):
        old = self.data.get(key, None)
        if old:
            new = old + value
        else:
            new = value
        self.data.update({key: new})

    def _search_parent_include(self):
        for field in fields(self.get_from):
            if field.name in self.include:
                field_value = getattr(self.get_from, field.name)
                self._update_data(field.name, field_value)

    def _search_parent_exclude(self):
        for field in fields(self.get_from):
            if field.name not in self.include:
                field_value = getattr(self.get_from, field.name)
                self._update_data(field.name, field_value)

    def _get(self):
        if self.include and not self.exclude:
            self._search_parent_include()
        if self.exclude and not self.include:
            self._search_parent_exclude()

class Graphers(Grapher):
    def pie_chart(self, title:str, d:list[Reader], attrs:list[str], include:bool = True) -> str:
        graphing_dict = {}
        # args should be instances of Reader class
        for incoming in d:
            if include:
               graphing_dict.update(
                       **ChartData(get_from=incoming, include=attrs).data
                       ) 
            else:
               graphing_dict.update(
                       **ChartData(get_from=incoming, include=attrs).data
                       ) 
        # Assigns the keys of graphing_dict to the labels, and values of those keys to data for the graph
        # For pie charts all the data must be positive
        data = [abs(x) for x in graphing_dict.values()]
        labels = graphing_dict.keys()
        # Generate a generic figure and plot
        wedges, texts, autotexts = self.axs.pie(
                                            data, 
                                            autopct='%1.1f%%',
                                            )
        legend_labels = [f"{lab}: {abs(value)}" for lab, value in graphing_dict.items()]
        self.axs.legend(wedges, legend_labels, title=title, loc='center left', bbox_to_anchor=(0.9,0,0.5,1))
        self.axs.set_title(title)
        final_path = path.join(self.save_folder, f"{title}.png")
        plt.tight_layout()
        plt.savefig(final_path)
        return final_path


if __name__ == "__main__":
    hist_data = HistorySQL().getrow(search=UseDates().stdatestr)
    include = ['groceries', 'gas', 'eattingout', 'vices', 'rent', 'bill', 'investing', 'otherexpend']
    Graphers().pie_chart(title='Expenditure', attrs=include, include=True, d=[hist_data])
