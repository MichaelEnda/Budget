from datetime import date
from os import path, mkdir
from re import sub
from tempfile import gettempdir
from dataclasses import dataclass, field
import matplotlib.pyplot as plt
from numpy import ndarray
from copy import deepcopy

from .utils import Colors

@dataclass
class GenericPlot:
    title: str= field(default_factory=str)
    xlabel: str = field(default_factory=str)
    ylabel: str = field(default_factory=str)
    ax: ndarray = field(init=False)
    fig: plt.Figure = field(init=False)
    lgd_data: list[plt.Line2D] = field(default_factory=list)
    _fullplot: tuple = field(default=plt.subplots(figsize=(8, 6)))

    def __post_init__(self):
        self.fig, self.ax = self._fullplot

    def _draw(self):
        self.ax.legend(handles=self.lgd_data)
        plt.draw()

    def show(self):
        self._draw()
        plt.show()

    def save(self, location:str):
        self._draw()
        plt.savefig(location)

@dataclass
class MainChart(GenericPlot):
    x_data: list = field(default_factory=list)
    y_data: list = field(default_factory=list)

    def _format_x_for_dates(self):
        if all([type(x) is date for x in self.x_data]):
            plt.xticks(rotation=45)

    def plot(self):
        self.ax.plot(self.x_data, self.y_data)
        self._format_x_for_dates()
        plt.title(self.title)
        plt.xlabel(self.xlabel)
        plt.ylabel(self.ylabel)


@dataclass
class SecondaryLines:
    data: list = field(default_factory=list)
    label: str = field(default="")
    color: str = field(default="k")
    vertical: bool = field(default=True)

    def _add_vert(self, base_chart: MainChart):
        for x in self.data:
            base_chart.ax.axvline(x, linestyle="--", color=self.color)
        return base_chart

    def _add_horiz(self, base_chart: MainChart):
        for x in self.data:
            base_chart.ax.axhline(x, linestyle="--", color=self.color)
        return base_chart

    def _add_lgd_data(self, base_chart: MainChart):
        base_chart.lgd_data.append(
                plt.Line2D([0], [0], linestyle="--", label=self.label, color=self.color)
                )

    def plot(self, base_chart: MainChart):
        if self.vertical:
            self._add_vert(base_chart)
        else:
            self._add_horiz(base_chart)
        self._add_lgd_data(base_chart)
        return base_chart



class LineChart(object):
    def __init__(self):
        self.base_plot = MainChart()
        self.colors = Colors()
        
    def set_labels(self, title:str, xlabel:str, ylabel:str):
        self.base_plot.title = title
        self.base_plot.xlabel = xlabel
        self.base_plot.ylabel = ylabel

    def plot_main_line(self, x_data: list, y_data: list):
        self.base_plot.x_data = x_data
        self.base_plot.y_data = y_data
        self.base_plot.plot()

    def _gen_secondary_lines(self, data:list, label:str, color:str | None = None, vert: bool = True):
        if not color:
            color = self.colors.color
        vert_lines = SecondaryLines(
                data=data,
                label=label,
                color=color,
                vertical=vert
                )
        return vert_lines

    def plot_secondary_lines(self, 
                             data: list[list],
                             labels:list[str], 
                             chart: MainChart | None = None,
                             color: str | None = None, 
                             vert: bool = True
                             ) -> MainChart:
        if chart:
            new_chart = deepcopy(chart)
        else:
            new_chart = deepcopy(self.base_plot)
        for label, lndata in zip(labels, data):
            sec_lines = self._gen_secondary_lines(
                    data = lndata,
                    label = label,
                    vert= vert
                    )
            new_chart = sec_lines.plot(base_chart=new_chart)
        return new_chart


    def show(self, chart:MainChart):
        chart.plot()
        chart.show()

    def save(self, chart:MainChart) -> str:
        chart.plot()
        save_folder = path.join(gettempdir(), 'budget')
        if not path.isdir(save_folder):
            mkdir(save_folder)
        # removes non-alphanuermic characters and replaces consecturive hyphens or whitespace with hyphens
        safe_name = sub(r'[^\w\s-]', '', chart.title.strip())
        safe_name = sub(r'[-\s]+', '-', safe_name)
        final_path = path.join(save_folder, f"{chart.title}.png")
        chart.save(final_path)
        return final_path
       
