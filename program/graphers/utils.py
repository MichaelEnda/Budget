from abc import ABC, abstractmethod
from dataclasses import dataclass
from os import path, mkdir
from tempfile import gettempdir
import matplotlib.pyplot as plt



class Grapher():
    # class attribute
    save_folder = path.join(gettempdir(), 'budget')
    def __init__(self, subplot:tuple[int, int]=(1,1)):
        # instance attributes
        self.fig, self.axs = plt.subplots(*subplot)
        if not path.isdir(self.save_folder):
            mkdir(self.save_folder)
    @property
    def setup(self):
        return self.fig, self.axs

    def save(self, name:str):
        final_path = path.join(self.save_folder, f"{name}.png")
        plt.savefig(final_path)


class Colors():
    def __init__(self):
        self._colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']

    @property
    def color(self):
        ret_col = self._colors.pop(0)
        self._colors.append(ret_col)
        return ret_col

