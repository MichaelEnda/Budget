from . import MaintSQL, MaintCheckSQL, MilesSQL, GasSQL, RecurringSQL, OneTimeSQL, HistorySQL, SpendingSQL

from .sql_access import SQL_access

class GenTables(SQL_access):
    def check_table(self, table) -> bool:
        with self.con as con:
            cmd = f"""SELECT count(name) FROM sqlite_master WHERE type='table' AND name='{table}' """
            output = con.cursor().execute(cmd).fetchall()[0][0]
        if output == 1:
            return True
        elif output < 1:
            return False
        else:
            raise ValueError("Something fucked up the the check_table function")

    def main(self):
        if not self.check_table('spending'):
            SpendingSQL().gen_spending() 
        if not self.check_table('onetime'):
            OneTimeSQL().gen_onetime()
        if not self.check_table('recurring'):
            RecurringSQL().gen_recurring()
        if not self.check_table('history'):
            HistorySQL().gen_history()
        if not self.check_table('gas'):
            GasSQL().generate_gas_table()
        if not self.check_table('maint'):
            MaintSQL().gen_maint() 
        if not self.check_table('maintcheck'):
            MaintCheckSQL().gen_maint_check()
        if not self.check_table('miles'):
            MilesSQL().gen_miles_table()


if __name__ == "__main__":
    GenTables().main()
