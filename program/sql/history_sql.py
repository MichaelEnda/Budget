import datetime as dt
from dataclasses import astuple, dataclass, InitVar, field, fields


from .spending_sql import SpendingSQL
from .sql_access import SQL_access
from .sql_utils import Reader, SQLUtilFuncs as SQLUtil, Writer
from ..utils import UseDates


class HistorySQL(SQL_access):
    """
    CREATE TABLE history (
            ID INTEGER,
            startdate DATE NOT NULL UNIQUE,
            groceries REAL NOT NULL,
            gas REAL NOT NULL,
            eattingout REAL NOT NULL,
            vices REAL NOT NULL,
            rent REAL NOT NULL,
            bill REAL NOT NULL,
            investing REAL NOT NULL,
            otherexpend REAL NOT NULL,
            wages REAL NOT NULL,
            scholarships REAL NOT NULL,
            otherincome REAL NOT NULL,
            netincome REAL GENERATED ALWAYS AS
                (wages + scholarships + otherincome),
            netexpend REAL GENERATED ALWAYS AS
                (groceries + gas + eattingout + vices + rent + bill + investing + otherexpend),
            savings REAL,
            PRIMARY KEY (ID ASC)
            )
    TODO: Write tests, add netincome, netexpend post_init vars to History write, add spending as an init var
    """

    @dataclass
    class HistoryWrite(Writer):
        stdate: dt.date = field(default=(UseDates.stdate))
        groceries: float = field(default=0)
        gas: float = field(default=0)
        eattingout: float = field(default=0)
        vices: float = field(default=0)
        rent: float = field(default=0)
        bill: float = field(default=0)
        investing: float = field(default=0)
        otherexpend: float = field(default=0)
        wages: float = field(default=0)
        scholarships: float = field(default=0)
        otherinc: float = field(default=0)
        netinc: float = field(default=0, init=False)
        netexp: float = field(default=0, init=False)
        savings: float = field(init=False)
        _payback: float = field(default=0)


        def calculate_net(self):
            self.savings = SpendingSQL().get_savings()
            self.netinc = round(self.wages + self.scholarships + self.otherinc, 2)
            raw_expend = self.groceries + \
                    self.gas + \
                    self.eattingout + \
                    self.vices + \
                    self.rent + \
                    self.bill + \
                    self.investing + \
                    self.otherexpend 
            self.netexp = round(raw_expend + self._payback, 2)
            if type(self.stdate) == str:
                self.stdate = SQLUtil().str_to_date(self.stdate)


    @dataclass
    class HistoryRead(Reader):
        rowid: int
        stdate: dt.date 
        groceries: float
        gas: float
        eattingout: float
        vices: float
        rent: float
        bill: float
        investing: float
        otherexpend: float
        wages: float
        scholarships: float
        otherinc: float
        netinc: float
        netexp: float
        savings: float

        def pdf_write_spending(self):
            dont_want = [
                    'rowid', 
                    'stdate',
                    'wages',
                    'scholarships',
                    'otherinc',
                    'netinc',
                    'savings'
                    ]
            for field in fields(self):
                if field.name not in dont_want: 
                    yield f"{field.name}: {round(getattr(self, field.name), 2)}"

        def pdf_write_income(self):
            want = [
                    "wages",
                    "scholarships",
                    "otherinc",
                    "netinc"
                    ]
            for field in fields(self):
                if field.name in want: 
                    yield f"{field.name}: {round(getattr(self, field.name), 2)}"

    def gen_table(self):
        print('Creating History Table.')
        with self.con as con:
            cmd = """
            CREATE TABLE history (
                    ID INTEGER,
                    startdate DATE NOT NULL UNIQUE,
                    groceries REAL NOT NULL,
                    gas REAL NOT NULL,
                    eattingout REAL NOT NULL,
                    vices REAL NOT NULL,
                    rent REAL NOT NULL,
                    bill REAL NOT NULL,
                    investing REAL NOT NULL,
                    otherexpend REAL NOT NULL,
                    wages REAL NOT NULL,
                    scholarships REAL NOT NULL,
                    otherincome REAL NOT NULL,
                    netincome REAL NOT NULL,
                    netexpend REAL NOT NULL,
                    savings REAL NOT NULL,
                    PRIMARY KEY (ID ASC)
                    )
            """
            con.cursor().execute(cmd)

    def rounding_trigger(self):
        with self.con as con:
            sqlcmd = """
            CREATE TRIGGER round_reals
            BEFORE INSERT ON history
            FOR EACH ROW
            BEGIN
                NEW.startdate = ROUND(NEW.startdate, 2);
                NEW.groceries = ROUND(NEW.groceries, 2);
                NEW.gas = ROUND(NEW.gas, 2);
                NEW.eattingout = ROUND(NEW.eattingout, 2);
                NEW.vices = ROUND(NEW.vices, 2);
                NEW.rent = ROUND(NEW.rent, 2);
                NEW.bill = ROUND(NEW.bill, 2);
                NEW.investing = ROUND(NEW.investing, 2);
                NEW.otherexpend  = ROUND(NEW.otherexpend , 2);
                NEW.wages = ROUND(NEW.wages, 2);
                NEW.scholarships = ROUND(NEW.scholarships, 2);
                NEW.otherincome  = ROUND(NEW.otherincome , 2);
                NEW.netincome = ROUND(NEW.netincome, 2);
                NEW.netexpend = ROUND(NEW.netexpend, 2);
                NEW.savings = ROUND(NEW.savings, 2);
            END
            """
            con.cursor().execute(sqlcmd)

    def gen_history(self):
        self.gen_table()
        self.rounding_trigger()

    def add_or_update(self, data:HistoryWrite) -> None:
        with self.con as con:
            sqlcmd = """
            INSERT INTO history (startdate, groceries, gas, eattingout, vices, 
                                 rent, bill, investing, otherexpend, wages, scholarships,
                                 otherincome, netincome, netexpend, savings) VALUES 
            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            ON CONFLICT(startdate) DO UPDATE SET
            startdate = excluded.startdate,
            groceries = excluded.groceries,
            gas = excluded.gas,
            eattingout = excluded.eattingout,
            vices = excluded.vices, 
            rent = excluded.rent,
            bill = excluded.bill,
            investing = excluded.investing,
            otherexpend = excluded.otherexpend,
            wages = excluded.wages,
            scholarships = excluded.scholarships,
            otherincome = excluded.otherincome,
            netincome = excluded.netincome,
            netexpend = excluded.netexpend,
            savings = excluded.savings
            """
            con.cursor().execute(sqlcmd, astuple(data)[:-1])

    def getrow(self, search:int | str) -> HistoryRead:
        if type(search) is int:
            with self.con as con:
                sqlcmd = """
                SELECT * FROM HISTORY WHERE ID = ?
                """
                data = con.cursor().execute(sqlcmd, (search,)).fetchone()
        if type(search) is str:
            with self.con as con:
                sqlcmd = """
                SELECT * FROM HISTORY WHERE startdate = ?
                """
                data = con.cursor().execute(sqlcmd, (search,)).fetchone()
        else:
            raise ValueError(f"{type(search)} is not a valid search type.")
        return HistorySQL.HistoryRead(*data)

    def gettable(self) -> list[HistoryRead]:
        with self.con as con:
            sqlcmd = """
            SELECT * FROM history
            """
            data = con.cursor().execute(sqlcmd).fetchall()
        return [self.HistoryRead(*row) for row in data]


    def _get_avg_income(self) -> float:
        with self.con as con:
            sqlcmd = """
            SELECT AVG(wages) + AVG(scholarships )FROM history
            """
            raw = con.cursor().execute(sqlcmd).fetchone()[0]
        return round(float(raw), 2)

    def _get_avg_spending(self) -> float:
        with self.con as con:
            sqlcmd = """
            SELECT AVG(netexpend) FROM history
            """
            raw = con.cursor().execute(sqlcmd).fetchone()[0]
        return round(float(raw), 2)

    @property
    def saving_goal(self) -> float:
        with self.con as con:
            sqlcmd = """
            SELECT AVG(netexpend) FROM history ORDER BY netexpend DESC LIMIT 3
            """
            data = con.cursor().execute(sqlcmd).fetchone()
        return round(-data[0], 2)


    @property
    def avgs(self) -> tuple[float, float]:
        """Returns tuple of floats: (income, expend)"""
        return self._get_avg_income(), self._get_avg_spending()

    @property
    def savings(self) -> float:
        """Returns savings as float"""
        curdate = UseDates.stdate
        with self.con as con:
            sqlcmd = """
            SELECT savings FROM history WHERE startdate = ?
            """
            raw = con.cursor().execute(sqlcmd, (curdate,)).fetchone()[0]
        return round(raw, 2)


