from datetime import datetime
import os
import csv
from typing import Dict, Iterable
from .spending_sql import SpendingSQL
from .maint_sql import MaintSQL
from .gas_sql import GasSQL

class NewImporter():
    """
    This is to import all the raw data to the database
    """

    def __init__(self, 
                 input_loc= os.path.join(os.getcwd(), 'raw_export_files'),
                 test: bool = False,
                 test_data: list = [] 
                 ):
        self.input_loc = input_loc
        self.file_list = os.listdir(self.input_loc)
        self.test = test
        self.test_data = test_data

    def file_sorter(self, data, fname):
        """
        This funtion automatically calls the sorter for the respective file types
        """
        check_line = next(data) 
        if check_line[0] == 'Account Number': 
            return self.ufcu_sorter(data)
        if check_line[0] == 'price of tank':
            return self.car_sorter(data)
        else:
            raise ValueError(f"{fname} of unsupported filetype, please update NewImporter.file_sorter")

    def car_sorter(self, data) -> tuple[str, list, list]:
        """
        Returns a tuple of (gas data, maint data). No longer automatically writes to DB
        """
        gas_data = []
        maintenance_data = []
        for row in data:
            if row[0] == "maintenance":
                maint_row = row[1:4]
                test = [True if col and col != "NA" else False for col in maint_row]
                if all(test):
                    maintenance_data.append(maint_row)
            else:
                gas_row = row[0:4]
                # tests the incoming row of data to ensure that it is a valid row
                test = [True if col and col != 'NA' else False for col in
                    gas_row]
                if all(test):
                    gas_data.append(gas_row)
        return ('car', gas_data, maintenance_data)

    def ufcu_sorter(self, data) -> tuple[str, Iterable, str]:
        check_line = next(data)
        # Checks if saving account
        if check_line[0] == 'XX6332XXXXXXS01':
            return ('ufcu', data, 'UFirst Savings')
        # check if checking account
        if check_line[0] == 'XX6332XXXXXXS90':
            return ('ufcu', data, 'UFirst Checking')
        if check_line[0] == 'XX6332XXXXXXL90':
            return ('ufcu', data, 'UFirst Credit Card')
        else:
            raise ValueError(f"{self.curfile} failed to import. Update importer.ufcu_sorter")

    def maintenance_sorter(self, data: list) -> Dict[str, MaintSQL.MaintWrite]:
        """
        Accepts the incoming data from car sorter and returns a dict of the 
        most recent maintenances performed
        """
        in_maint = {}
        for row in data:
            row = MaintSQL.MaintWrite(
                    maint=row[0],
                    date=row[1],
                    miles=row[2]
                        )
            row.validate()
            check = in_maint.get(row.maint, None)
            if not check:
                in_maint.update({row.maint: row})
            else:
                # These seperate if statements decouple the updating of miles and date 
                # so that the row in maintsql has the most recent of each irrespecive of
                # if they were stored in the same row in the spreadsheet.
                if row.date >= check.date:
                    check.date = row.date
                    in_maint.update({row.maint: check})
                if row.miles >= check.miles:
                    check.miles = row.miles
                    in_maint.update({row.maint: check})
        return in_maint

    def maintenance_importer(self, data: dict[str, MaintSQL.MaintWrite], con = None):
        if con:
            db = MaintSQL(con=con)
        else:
            db = MaintSQL()
        # This block test against the current table
        table_maint = {row.maint: row for row in db.gettable()}
        for incoming_maint, incoming_data in data.items():
            check_table = table_maint.get(incoming_maint, None)
            # Below is true as if there is not entry for the maint the table needs update
            table_need_update = True
            if check_table:
                # Below validates that the incoming data is new (for both miles and date) than stored data
                table_need_update = (
                    float(check_table.miles) <= float(incoming_data.miles) or
                    check_table.date <= incoming_data.date
                    )
            # Incoming data should always be validated, this is a check and also ensure proper write formatting
            incoming_data_valid = incoming_data.validate()
            if table_need_update and incoming_data_valid:
                db.add_or_update(incoming_data)

        print('Importing maintenance')

    def gas_importer(self, data: list, con = None):
        db = GasSQL()
        if con:
            db = GasSQL(con=con)
        cost = 0
        gallons = 0
        miles = 0
        for row in data:
            row_date = datetime.strptime(row[2], "%m/%d/%Y").date()
            row = GasSQL.GasWrite(
                    date = row_date,
                    cost = float(row[0]),
                    gallons= float(row[1]),
                    miles = float(row[3])
                    )
            if row.validate():
                cost += row.cost
                gallons += row.gallons
                miles += row.miles
        print('Adding Gas')
        db.add_or_update(GasSQL.GasWrite(
            cost= cost,
            gallons= gallons,
            miles= miles
            ))

    def ufcu_change_check(self, row: list) -> float:
        if row[4]: return -float(row[4])
        if row[5]: return float(row[5])
        else:
            raise ValueError('Error importing UFCU values')

    def ufcu_importer(self, data: Iterable, act: str, con = None
                      ) -> list[SpendingSQL.SpendingWrite]:
        db = SpendingSQL()
        if con:
            db = SpendingSQL(con=con)
        write_data = []
        for row in data:
            if row[6] == "Posted":
                write_row = SpendingSQL.SpendingWrite(
                        acc= act,
                        date = row[1],
                        desc= row[3],
                        change = self.ufcu_change_check(
                            row= row
                            ),
                        bal = float(row[7])
                        )
                write_data.append(write_row)
        print(f'Adding {act} data')
        db.add_many(write_data)
        return write_data

    def import_sorted(self, data: tuple) -> None:
        if data[0] == 'car':
            _, gas_data, maint_data = data
            maint_data = self.maintenance_sorter(maint_data)
            self.gas_importer(gas_data)
            self.maintenance_importer(maint_data)
        if data[0] == 'ufcu':
            _, ufcu_data, act = data
            self.ufcu_importer(data=ufcu_data, 
                               act=act
                               )

    def main(self):
        for file in self.file_list:
            print(file)
            self.curfile = file
            fileloc = os.path.join(self.input_loc, file)
            with open(fileloc, 'r') as infile:
                reader = csv.reader(infile, delimiter=',')
                sorted_data = self.file_sorter(reader, file)
                self.import_sorted(sorted_data)

            os.remove(fileloc)

if __name__ == "__main__":
    NewImporter().main()
