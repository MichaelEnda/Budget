from dataclasses import dataclass, astuple, field, InitVar
from datetime import date as dt
from datetime import timedelta

from .sql_access import SQL_access
from .maint_sql import MaintSQL
from .miles_sql import MilesSQL
from ..utils import UseDates

class MaintCheckSQL(SQL_access):
    """
    ID - Generated
    maint Foregin key -> Maint.maint
    daterecur
    milerecur
    """

    @dataclass
    class MaintCheckWrite:
        maint: str
        daterecur: int | str
        milerecur: int | str

        def validate(self) -> bool:
            maint = isinstance(self.maint, str) and len(self.maint) > 0
            try:
                self.daterecur = int(self.daterecur)
            except ValueError:
                return False
            date = isinstance(self.daterecur, int)
            try:
                self.milerecur = int(self.milerecur)
            except ValueError:
                return False
            mile = isinstance(self.milerecur, int)
            return all([maint, date, mile])

    @dataclass
    class MaintCheckRead:
        rowid: int
        maint: str
        daterecur: int
        milerecur: int

    @dataclass
    class ReportData:
        maint: str
        last_date: dt
        next_date: dt = field(init=False)
        last_miles: int
        next_miles: int = field(init=False)
        daterecur: InitVar[timedelta]
        milerecur: InitVar[int]

        def __post_init__(self, daterecur, milerecur):
            self.next_date = self.last_date + daterecur
            self.next_miles = self.last_miles + milerecur

        def check_date(self, 
                             curdate: dt=UseDates().enddate,
                             ) -> bool:
            return self.next_date <= curdate 

        def check_miles(self,
                        curmiles: int
                        ) -> bool:
            # curmiles is not set as default because the 'miles' table might not exists on init
            if not curmiles:
                curmiles = MilesSQL().get_miles(search=UseDates.enddate)
            return self.next_miles <= curmiles

        @property
        def need_update(self):
            return self.check_date() or self.check_miles()


    @property
    def default(self):
        check = [
                # maint,      date, miles
                ('oil change', 183, 5000),
                ('oil filter change', 183, 5000),
                ('coolant change', 1826, 80000),
                # I don't have the last time I performed these in my google sheets
#                 ('wiper change', 183, 999999999),
#                 ('tire rotation', 730, 6000),
#                 ('tire change', 730, 65000)
                ]
        return [
                self.MaintCheckWrite(
                    maint=x[0],
                    daterecur=x[1],
                    milerecur=x[2]
                    ) for x in check
                ]

    def gen_maint_check(self):
        print('Creating maintenance check table')
        with self.con as con:
            sqlcmd = """
            CREATE TABLE maintcheck (
                    ID INTEGER,
                    maint TEXT UNIQUE,
                    daterecur INTEGER,
                    milerecur INTEGER,
                    PRIMARY KEY (ID ASC),
                    FOREIGN KEY (maint)
                        REFERENCES maint (maint)
                    )
            """
            con.cursor().execute(sqlcmd)

    def add_defaults(self):
        for x in self.default:
            self.add_or_update(x)

    def add_or_update(self, data:MaintCheckWrite) -> None:
        with self.con as con:
            sqlcmd = """
            INSERT INTO maintCheck (maint, daterecur, milerecur) 
            VALUES (?, ?, ?)
            ON CONFLICT (maint) DO UPDATE SET
                maint = excluded.maint,
                daterecur = excluded.daterecur,
                milerecur = excluded.milerecur
                """
            con.cursor().execute(sqlcmd, astuple(data))

    def update_row(self, rowid: int, updata: MaintCheckWrite) -> None:
        with self.con as con:
            sqlcmd = """
            UPDATE maintCheck SET (maint, daterecur, milerecur) =  
            (?, ?, ?) WHERE ID = ?
            """
            con.cursor().execute(sqlcmd, (*astuple(updata), rowid))

    def getrow(self, search: int | str) -> MaintCheckRead | None:
        if isinstance(search, str):
            sqlcmd = """
            SELECT * FROM maintCheck WHERE maint = ?
            """
        elif isinstance(search, int):
            sqlcmd = """
            SELECT * FROM maintCheck WHERE ID = ?
            """
        else: raise ValueError(f"{type(search), search} is not a valid type to search maintcheck")
        with self.con as con:
            data = con.cursor().execute(sqlcmd, (search,)).fetchone()
        if data: return self.MaintCheckRead(*data)
        else: return None

    def gettable(self) -> list[MaintCheckRead]:
        with self.con as con:
            sqlcmd = """
            SELECT * FROM maintCheck
            """
            data = con.cursor().execute(sqlcmd).fetchall()
        return [self.MaintCheckRead(*row) for row in data]

    def check_table(self, 
                    curmiles: float, 
                    curdate: dt
                    ) -> dict[str, MaintSQL.MaintRead]:
        maint_sql = MaintSQL()
        check_data = self.gettable()
        needs_update = {}
        for check_row in check_data:
            maint_data = maint_sql.getrow(search=check_row.maint)
            new_miles = maint_data.miles + check_row.milerecur
            new_date = maint_data.date + timedelta(check_row.daterecur)
            if curmiles >= new_miles or curdate >= new_date:
                needs_update.update({check_row.maint: maint_data})
        return needs_update

    def report_data(self) -> list[ReportData]:
        return_list = []
        for check_data in self.gettable():
            maint_data = MaintSQL().getrow(search=check_data.maint)
            new = self.ReportData(
                    maint= maint_data.maint,
                    last_miles= maint_data.miles,
                    last_date = maint_data.date,
                    daterecur= timedelta(days=check_data.daterecur),
                    milerecur= check_data.milerecur,
                    )
            return_list.append(new)
        return return_list


if __name__ == "__main__":
    MaintCheckSQL().add_defaults()

