from dataclasses import dataclass, InitVar, field, astuple
from datetime import timedelta
from datetime import date as dt

from .sql_access import SQL_access
from .sql_utils import SQLUtilFuncs as SQLUtil
from ..utils import UseDates

class MaintSQL(SQL_access):
    """
    ID 
    maint 
    date 
    miles
    note
    """

    @dataclass
    class MaintWrite:
        maint: str = field(default="")
        date: dt | str = field(default=UseDates.enddate)
        miles: int | str = field(default=0)
        note: str = field(default="")

        def validate(self) -> bool:
            maint = isinstance(self.maint, str) and len(self.maint) > 0
            if type(self.date) is str:
                self.date = SQLUtil().str_to_date(self.date)
            date = isinstance(self.date, dt)
            try:
                self.miles = int(self.miles)
            except ValueError:
                return False
            miles = isinstance(self.miles, int)
            note = isinstance(self.note, str)
            return all([maint, date, miles, note])

    @dataclass
    class MaintRead:
        rowid: int
        maint: str
        date: dt
        miles: int
        note: str

        def __iter__(self):
            return iter([str(x) for x in astuple(self)])

    @dataclass
    class NeedsUpdate:
        maint: str
        last_date: dt
        next_date: dt = field(init=False)
        last_miles: int
        next_miles: int = field(init=False)
        daterecur: InitVar[timedelta]
        milerecur: InitVar[int]

        def __post_init__(self, daterecur, milerecur):
            self.next_date = self.last_date + daterecur
            self.next_miles = self.last_miles + milerecur

    def gen_maint(self):
        print('Creating Maintenance table')
        with self.con as con:
            sqlcmd = """
            CREATE TABLE maint (
                    ID INTEGER,
                    maint TEXT NOT NULL UNIQUE,
                    date DATE NOT NULL,
                    miles INT NOT NULL,
                    note TEXT NOT NULL,
                    PRIMARY KEY (ID ASC)
                    )
            """
            con.cursor().execute(sqlcmd)

    maint_check = {
            'oil change': (timedelta(days=183), 5000, 'Full synthetic 0w-20'
                + ' oil'),
            'oil filter change': (timedelta(days=183), 5000, 'Unknown oil'
                + 'filter'),
            'coolant change': (timedelta(days=1826), 80000, 'Zerex Asian'
                + 'Vehcile antifreeze coolant 50/50'),
            'wiper change': (timedelta(days=183), 99999999999, 'Drivers side'
                + '24", passenger side 17", rear 12"'),
            'tire rotation': (timedelta(730), 6000, 'balance tires and wear'),
            'tire change': (timedelta(730), 65000, 'check reciept for warrenty')
            }

    def add_or_update(self, data:MaintWrite) -> None:
        with self.con as con:
            sqlcmd = """
            INSERT INTO maint (maint, date, miles, note) 
            VALUES (?, ?, ?, ?)
            ON CONFLICT(maint) DO UPDATE SET
                maint = excluded.maint,
                date = excluded.date,
                miles = excluded.miles,
                note = excluded.note
            """
            con.cursor().execute(sqlcmd, astuple(data))

    def update_row(self, rowid: int, updata: MaintWrite) -> None:
        with self.con as con:
            sqlcmd = """
            UPDATE maint SET (maint, date, miles, note) =  
            (?, ?, ?, ?) WHERE ID = ?
            """
            con.cursor().execute(sqlcmd, (astuple(updata), rowid))

    def getrow(self, search: int | str) -> MaintRead:
        if isinstance(search, str):
            sqlcmd = """
            SELECT * FROM maint WHERE maint = ?
            """
        elif isinstance(search, int):
            sqlcmd = """
            SELECT * FROM maint WHERE ID = ?
            """
        else: raise ValueError(f"{type(search), search} is not a valid type to search maintenance")
        with self.con as con:
            # This try block is to prevent a break if no maint foregin key is found
            data = con.cursor().execute(sqlcmd, (search,)).fetchone()
        if data: 
            return self.MaintRead(*data)
        # I don't think this is needed, I only use this in the tui which can only search existing data
        else:
            raise ValueError(f"Cannot find searched maint: {search}")

    def gettable(self) -> list[MaintRead]:
        with self.con as con:
            sqlcmd = """
            SELECT * FROM maint
            """
            data = con.cursor().execute(sqlcmd).fetchall()
        return [self.MaintRead(*row) for row in data]

    # I moved check table to maint_check_sql
