import datetime

from .sql_access import SQL_access
from .history_sql import HistorySQL
from ..utils import UseDates

class MilesSQL(SQL_access):
    def __init__(self, 
                 stdate: datetime.date = UseDates.stdate,
                 enddate: datetime.date = UseDates.enddate):
        self.stdate = stdate
        self.enddate = enddate 
        super().__init__()

    @property
    def curmiles(self) -> int | None:
        cur_search = self.get_miles(search=self.enddate)
        if cur_search:
            return cur_search
        else:
            return None

    def gen_miles_table(self):
        with self.con as con:
            sqlcmd = """
            CREATE TABLE miles (
                    ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    hist_id INT,
                    date DATE UNIQUE,
                    mile INTEGER DEFAULT 0,
                    FOREIGN KEY (hist_id) REFERENCES history (id)
                    )
            """
            con.execute(sqlcmd)

    def add_or_update(self, data: int):
        hist_id = HistorySQL().getrow(search= UseDates().stdatestr).rowid
        with self.con as con:
            sqlcmd = """
            INSERT INTO miles (hist_id, date, mile)
            VALUES (?, ?, ?)
            ON CONFLICT(date) DO UPDATE SET
                hist_id = excluded.hist_id,
                date = excluded.date,
                mile = excluded.mile
            """
            con.execute(sqlcmd, (hist_id, self.enddate, data))

    def get_miles(self, search: int | datetime.date=UseDates.enddate) -> int:
        if type(search) == int:
            sqlcmd = """
            SELECT mile FROM miles WHERE rowid = ?
            """
        if type(search) == datetime.date:
            sqlcmd = """
            SELECT mile FROM miles WHERE date = ?
            """
        else:
            raise ValueError(f"{type(search)} is not a valid search type for miles. Please use int or date")
        with self.con as con:
            row = con.execute(sqlcmd, (search, )).fetchone()
        return row[0]

if __name__ == "__main__":
    MilesSQL().add_or_update(data=6969)
