from dataclasses import dataclass, astuple, field
from datetime import date as dt

from .sql_access import SQL_access
from .sql_utils import Reader, SQLUtilFuncs as SQLUtil, Writer
from ..utils import UseDates

class OneTimeSQL(SQL_access):
    @dataclass
    class OneTimeWrite(Writer):
        goal: str = field(default="")
        cost: str | float = field(default=0)
        date: str | dt = field(default=UseDates().enddatestr)
        
             
        def validate(self):
            goal = isinstance(self.goal, str) and len(self.goal) > 0
            try:
                self.cost = float(self.cost)
            except ValueError:
                return False
            cost = isinstance(self.cost, float)
            if type(self.date) is str:
                self.date = SQLUtil().str_to_date(self.date)
            date = isinstance(self.date, dt)
            return all([goal, cost, date])

    @dataclass
    class OneTimeRead(Reader):
        rowid: int
        goal: str
        cost: float
        date: dt

    def gen_onetime(self):
        print('Creating the OneTime table.')
        with self.con as con:
            cmd = """
            CREATE TABLE onetime (
                ID INTEGER,
                goal TEXT NOT NULL UNIQUE,
                cost REAL NOT NULL,
                date DATE,
                PRIMARY KEY (ID ASC))
            """
            con.cursor().execute(cmd)

    def add_or_update(self, data:OneTimeWrite) -> None:
        with self.con as con:
            cmd = """
            INSERT into onetime (Goal, Cost, Date) 
            VALUES (?, ?, ?)
            ON CONFLICT(Goal) DO UPDATE SET
                goal = excluded.goal,
                cost = excluded.cost,
                date = excluded.date
            """
            con.cursor().execute(cmd, astuple(data))

    def gettable(self) -> list[OneTimeRead]:
        with self.con as con:
            cmd = """
            SELECT * FROM onetime 
            """
            data = con.cursor().execute(cmd).fetchall()
        return [self.OneTimeRead(*row) for row in data]

    def getrow(self, rowid: int | str) -> OneTimeRead:
        rowid = str(rowid)
        with self.con as con:
            cmd = """
            SELECT * FROM onetime WHERE rowid = ?
            """
            row = con.cursor().execute(cmd, (rowid,)).fetchone()
        return self.OneTimeRead(*row)

    def get_cost_by_date(self, date: dt) -> float:
        with self.con as con:
            sqlcmd = """
            SELECT SUM(cost) FROM onetime WHERE date = ?
            """
            raw = con.cursor().execute(sqlcmd, (date, )).fetchone()[0]
        if raw:
            return round(raw, 2)
        else:
            return 0.0

    def get_headers(self):
        with self.con as con:
            cmd = """
            PRAGMA table_info(onetime)
            """
            data = con.cursor().execute(cmd).fetchall()
        return [x[1].title() for x in data]


    def removerow(self, rowid: int | str) -> None:
        rowid = str(rowid)
        with self.con as con:
            cmd = """
            DELETE FROM onetime WHERE ID = ?
            """
            con.cursor().execute(cmd, (rowid,))

