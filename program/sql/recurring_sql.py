from datetime import date as dt
from dataclasses import astuple, dataclass, field
from dateutil.relativedelta import relativedelta 


from .sql_access import SQL_access
from .sql_utils import Reader, SQLUtilFuncs as SQLUtil, Writer
from ..utils import UseDates

class RecurringSQL(SQL_access):
    @dataclass
    class RecurringWrite(Writer):
        """If period is given in months, month = True, else period is given in days"""
        goal: str = field(default="")
        cost: float | str = field(default=0)
        date: dt | str = field(default=UseDates().enddatestr)
        period: int | str = field(default=0)
        month: bool = field(default=True)


        def validate(self):
            goal = isinstance(self.goal, str) and len(self.goal) > 0
            try:
                self.cost = float(self.cost)
            except ValueError:
                return False
            cost = isinstance(self.cost, float) and self.cost > 0
            if type(self.date) is str:
                self.date = SQLUtil().str_to_date(self.date)
            date = isinstance(self.date, dt) 
            try:
                self.period = int(self.period)
            except ValueError:
                return False
            period = isinstance(self.period, int) 
            month = isinstance(self.month, bool)
            return all([goal, cost, date, period, month])


    @dataclass
    class RecurringRead(Reader):
        rowid: int
        goal: str
        cost: float
        date: dt
        period: int
        month : bool

    def gen_recurring(self):
        print('Creating Recurring Table.')
        with self.con as con:
            cmd = """
            CREATE TABLE recurring (
                    ID INTEGER,
                    goal TEXT NOT NULL UNIQUE,
                    cost REAL NOT NULL,
                    date DATE NOT NULL,
                    period INT NOT NULL,
                    month INT NOT NULL,
                    PRIMARY KEY (ID ASC)
                    )
            """
            con.cursor().execute(cmd)

    # I think that python will convert 0 to false automatically, if it fails check this out
    def add_or_update(self, writedata: RecurringWrite) -> None:
        with self.con as con:
            cmd = """
            INSERT INTO recurring (goal, cost, date, period, month) 
            VALUES (?, ?, ?, ?, ?)
            ON CONFLICT(goal) DO UPDATE SET
                goal = excluded.goal,
                cost = excluded.cost,
                date = excluded.date,
                period = excluded.period,
                month = excluded.month
            """
            con.cursor().execute(cmd, astuple(writedata))

    def gettable(self) -> list[RecurringRead]:
        with self.con as con:
            cmd = """
            SELECT * FROM recurring
            """
            data = con.cursor().execute(cmd).fetchall()
        return [self.RecurringRead(*row) for row in data]

    def getrow(self, rowid: int | dt) -> RecurringRead:
        if type(rowid) is int:
            cmd = """
            SELECT * FROM recurring WHERE rowid = ?
            """
        if type(rowid) is dt:
            cmd = """
            SELECT * FROM recurring WHERE date = ?
            """
        else:
            raise TypeError(f"{type(rowid)} is not a valide search type for History table.")
        with self.con as con:
            row = con.cursor().execute(cmd, (rowid,)).fetchone()
        return self.RecurringRead(*row)

    def get_rows(self, search: int | dt) -> list[RecurringRead]:
        if type(search) is int:
            cmd = """
            SELECT * FROM recurring WHERE rowid = ?
            """
        if type(search) is dt:
            cmd = """
            SELECT * FROM recurring WHERE date = ?
            """
        else:
            raise TypeError(f"{type(search)} is not a valide search type for History table.")
        with self.con as con:
            rows = con.cursor().execute(cmd, (search,)).fetchall()
        return [self.RecurringRead(*row) for row in rows]

    def get_cost_from_date(self, date: dt) -> float:
        with self.con as con:
            sqlcmd = """
            SELECT SUM(cost) FROM recurring WHERE date = ?
            """
            cost = con.cursor().execute(sqlcmd, (date,)).fetchone()[0]
        if cost:
            return round(cost, 2)
        else:
            return 0.0

    def get_headers(self):
        with self.con as con:
            cmd = """
            PRAGMA table_info(recurring)
            """
            data = con.cursor().execute(cmd).fetchall()
        return [x[1].title() for x in data]

    def removerow(self, rowid: int) -> None:
        with self.con as con:
            cmd = """
            DELETE FROM recurring WHERE ID = ?
            """
            con.cursor().execute(cmd, (rowid,))

    def update_date(self) -> None:
        tabledata = self.gettable()
        cmd = """
        UPDATE recurring SET date = ? WHERE ID = ?
        """
        for row in tabledata:
            new_date = row.date
            while new_date < UseDates.enddate:
                if row.month:
                    new_date = new_date + relativedelta(months=row.period)
                else:
                    new_date = new_date + relativedelta(days=row.period)
            with self.con as con:
                con.cursor().execute(cmd, (new_date, row.rowid))

if __name__ == "__main__":
    RecurringSQL().update_date()

