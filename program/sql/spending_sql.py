from dataclasses import dataclass, astuple, InitVar, field
from datetime import datetime
from datetime import date as dt
from typing import Iterable

from .sql_access import SQL_access
from .sql_utils import Reader, SQLUtilFuncs as SQLUtils
from ..utils import UseDates

class SpendingSQL(SQL_access):
    @dataclass
    class SpendingWrite:
        acc: str
        date: dt | str
        desc: str
        change: float
        bal: float
        # Ideally this tag should be a literal of the taglist but considering how this is only called
        # when using the tui and the user can only input entries from the taglist it should be good.
        tag: str = field(default="")

        def __post_init__(self):
            if type(self.date) is str:
                self.date = SQLUtils().str_to_date(self.date)

    @dataclass
    class SpendingRead(Reader):
        acc: str
        indate: InitVar[dt]
        desc: str
        change: float
        bal: float
        tag: str
        date: str = field(init=False)

        def __post_init__(self, indate):
            self.date = datetime.strftime(indate, "%Y-%m-%d")


    def gen_spending(self):
        with self.con as con:
            cmd = """
            CREATE TABLE spending (
                    account TEXT,
                    date DATE,
                    description TEXT,
                    change REAL,
                    balance REAL,
                    tag TEXT,
                    PRIMARY KEY(account, date, description, change, balance)
                    )
            """
            con.cursor().execute(cmd)

    def add_row(self, data: SpendingWrite) -> None:
        with self.con as con:
            cmd = """
            INSERT OR IGNORE INTO spending (account, date, description, change, balance)
            VALUES (?, ?, ?, ?, ?)
            """
            con.cursor().execute(cmd, astuple(data)[:-1])

    def add_many(self, data: list[SpendingWrite]) -> None:
        with self.con as con:
            cmd = """
            INSERT OR IGNORE INTO spending (account, date, description, change, balance)
            VALUES (?, ?, ?, ?, ?)
            """
            write = []
            for row in data:
                write.append(astuple(row)[:-1])
            con.cursor().executemany(cmd, write)

    def update_rows(self, updata: list[tuple[str]]):
        with self.con as con:
            sqlcmd = """
            UPDATE spending SET tag = ? WHERE  
                account = ? AND
                date = ? AND
                change = ? AND
                balance = ?
                
            """
            con.cursor().executemany(sqlcmd, updata)

    def gettable(self) -> list[SpendingRead]:
        with self.con as con:
            cmd = """
            SELECT * FROM spending
            """
            data = con.cursor().execute(cmd).fetchall()
        return [self.SpendingRead(*row) for row in data]

    def getrow(self, rowid: int | str) -> SpendingRead:
        rowid = str(rowid)
        with self.con as con:
            cmd = """
            SELECT * FROM spending WHERE rowid = ?
            """
            row = con.cursor().execute(cmd, (rowid)).fetchone()
        return self.SpendingRead(*row)

    def get_between_date_hist_write(self, stdate: dt, enddate: dt) -> list[SpendingRead]:
        with self.con as con:
            cmd = """
            SELECT * FROM spending WHERE date >= ? AND
            date <= ?
            """
            data = con.cursor().execute(cmd, (stdate, enddate)).fetchall()
        hist_write_list = []
        for row in data:
            if row[6]:
                hist_write_list.append(self.SpendingRead(*row))
        return hist_write_list



    def spending_table_tui(self, st_date:dt, end_date:dt
                     ) -> Iterable[SpendingRead]:
        with self.con as con:
            cmd = """
            SELECT *, ROWID FROM spending WHERE date >= ? AND
            date <= ?
            """
            data = con.cursor().execute(cmd, (st_date, end_date)).fetchall()
        for row in data:
            row = self.SpendingRead(
                    acc = row[0],
                    indate = row[1],
                    desc= row[2],
                    change = row[3],
                    bal= row[4],
                    tag = row[5]
                    ) 
            yield row

    def get_headers(self):
        with self.con as con:
            cmd = """
            PRAGMA table_info(spending)
            """
            data = con.cursor().execute(cmd).fetchall()
        return [x[1].title() for x in data]

    def get_savings(self, datestr: str=UseDates().enddatestr):
        # I want to get the balance from the most recent row for each distinct account
        with self.con as con:
            sqlcmd = """
                SELECT t1.account, t1.date, t1.balance
                FROM spending t1
                INNER JOIN (
                        SELECT account, MAX(date)  AS max_date
                        FROM spending
                        WHERE date < ?
                        GROUP BY account
                ) t2 ON t1.account = t2.account AND t1.date = t2.max_date
                GROUP BY t1.account
            """
            savings = con.execute(sqlcmd, (datestr, )).fetchall()
            print(savings)
            total_savings = 0
            for account in savings:
                total_savings += account[2]
        return round(total_savings, 2)


if __name__ == "__main__":
    testsql = SpendingSQL(db=':memory:')
    test_write = testsql.SpendingWrite(
            acc='test',
            date=dt.today(),
            desc="test",
            change = -1.0,
            bal = 100.0,
            tag= "Gr"
            )
    print(test_write)
