import sqlite3
from datetime import datetime
from os import path, getcwd
from shutil import copy

from ..utils import UseDates

class SQL_access():
    def __init__(self, db = path.join(getcwd(), 'program', 'sql', 'budget.db')):
        self._db = db

    @property
    def db(self):
        return self._db

    @db.setter
    def db(self, database):
        self._db = database

    @property
    def con(self):
        return sqlite3.connect(self.db, detect_types=sqlite3.PARSE_DECLTYPES)

    def backup(self):
        back_up_folder = path.join(getcwd(), 'program', 'sql', 'backup')
        back_up_month = datetime.strftime(UseDates().stdatetime, "%m")
        backup_name = f"backup-{back_up_month}.db"
        copy(self.db, path.join(back_up_folder, backup_name))
