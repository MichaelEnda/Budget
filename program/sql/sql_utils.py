from dataclasses import dataclass, fields
from datetime import datetime
from datetime import date
from abc import ABC, abstractmethod

class SQLUtilFuncs(object):
    def str_to_date(self, datestr:str):
        date_fmts = ("%m/%d/%Y", "%Y-%m-%d")
        tempdate = None
        for fmt in date_fmts:
            try:
                tempdate = datetime.strptime(datestr, fmt)
            except ValueError:
                pass
        if tempdate:
            ndate = date(tempdate.year, tempdate.month, tempdate.day)
        else:
            raise ValueError("Date string incorrectly formatted")
        return ndate

# These are abstract classes that are the parent class of reader/writer dataclasses
# Used in the graphing module for type checking
@dataclass
class Reader(ABC):
    def __post_init__(self):
        # automatically rounds all pulls floats to two decimal places
        for field in fields(self):
            if field.type is float:
                raw = getattr(self, field.name)
                setattr(self, field.name, round(raw, 2))

@dataclass
class Writer(ABC):
    pass
