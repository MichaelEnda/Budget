from dateutil.relativedelta import relativedelta
from datetime import date as dt
from .recurring_sql import RecurringSQL
from ..utils import UseDates

class TempRecurring(RecurringSQL):
    def __init__(self):
        super().__init__()
        con = self.con.cursor()
        self.cur = con
        self.gen_recurring()

    def gen_recurring(self):
        cmd = """
        CREATE TEMPORARY TABLE temp_recurring (
                ID INTEGER,
                goal TEXT NOT NULL UNIQUE,
                cost REAL NOT NULL,
                date DATE NOT NULL,
                period INT NOT NULL,
                month INT NOT NULL,
                PRIMARY KEY (ID ASC)
                )
        """
        self.cur.execute(cmd)
        cmd = """INSERT INTO temp_recurring (goal, cost, date, period, month)
        SELECT goal, cost, date, period, month FROM recurring"""
        self.cur.execute(cmd)

    def gettable(self) -> list[RecurringSQL.RecurringRead]:
        cmd = """
        SELECT * FROM temp_recurring
        """
        data = self.cur.execute(cmd).fetchall()
        return [self.RecurringRead(*row) for row in data]

    def update_date(self, date:dt) -> list[tuple[str, dt]]:
        tabledata = self.gettable()
        cmd = """
        UPDATE temp_recurring SET date = ? WHERE ID = ?
        """
        ret_list = []
        for row in tabledata:
            new_date = row.date
            while new_date < date:
                if row.month:
                    new_date = new_date + relativedelta(months=row.period)
                else:
                    new_date = new_date + relativedelta(days=row.period)
            ret_list.append((row.goal, new_date))
            self.cur.execute(cmd, (new_date, row.rowid))
        return ret_list

    def get_cost_from_date(self, date: dt) -> float:
        sqlcmd = """
        SELECT SUM(cost) FROM temp_recurring WHERE date = ?
        """
        cost = self.cur.execute(sqlcmd, (date,)).fetchone()[0]
        if cost:
            return round(cost, 2)
        else:
            return 0.0



