import random
import string
import datetime




class GenImporterTestData:
   def gen_random_str(self, len) -> str:
    letters = string.ascii_letters + string.digits
    return ''.join(random.choice(letters) for i in range(len))

   def generate_random_date_string(self) -> str:
      year = datetime.date.today().year
      start_date = datetime.date(year, 1, 1)
      end_date = datetime.date(year, 12, 31)
      days_between_dates = (end_date - start_date).days
      random_number_of_days = random.randrange(days_between_dates)
      random_date = start_date + datetime.timedelta(days=random_number_of_days)
      return random_date.strftime("%Y-%m-%d")

   def gen_random_float(self, low: float, high: float) -> float:
      return round(random.uniform(low, high), 2)

   def generate_random_true_false(self, prob) -> bool:
      return random.random() < prob

   def _gen_ufcu_row(self, last_balance: float, act: str):
      row =[
            act,
            self.generate_random_date_string(),
            None,
            self.gen_random_str(20),
            # Index 4 is debit
            None,
            # Index 5 is credit
            None,
            'Posted',
            # Index 7 is balance
            None,
            ]
      if self.generate_random_true_false(0.5):
         row[4] = self.gen_random_float(-2000, 2000)
         row[7] = round(last_balance - row[4], 2)
      else:
         row[5] = self.gen_random_float(-2000, 2000)
         row[7] = round(last_balance + row[5], 2)
      return row


   def gen_ufcu_checking_data(self):
      yield ["Account Number","Post Date","Check","Description","Debit","Credit","Status","Balance"]
      last_balance = self.gen_random_float(-20000, 20000) 
      for x in range(100):
         row = self._gen_ufcu_row(act = 'XX6332XXXXXXS90',
                                  last_balance=last_balance)
         yield row
         last_balance = row[7]

   def gen_ufcu_saving_data(self):
      yield ["Account Number","Post Date","Check","Description","Debit","Credit","Status","Balance"]
      last_balance = self.gen_random_float(-20000, 20000) 
      for x in range(100):
         row = self._gen_ufcu_row(act = 'XX6332S01',
                                  last_balance=last_balance)
         yield row
         last_balance = row[7]

   def gen_ufcu_credit_data(self):
      yield ["Account Number","Post Date","Check","Description","Debit","Credit","Status","Balance"]
      last_balance = self.gen_random_float(-20000, 20000) 
      for x in range(100):
         row = self._gen_ufcu_row(act = 'VISA CRIMSON REWARDS VARIABLE',
                                  last_balance=last_balance)
         yield row
         last_balance = row[7]


   def gen_test_car_data(self):
      last_maint_milage = int(self.gen_random_float(0, 150000))
      test_maint_performed = [
           'test1',
           'test2',
           'test3',
           'test4',
           'test5',
           'test6',
            ]
      yield ["price of tank", "37.8", "Refill Date", "miles per tank"]
      for x in range(100):
         # This generates a maintenance row
         if self.generate_random_true_false(0.2):
            row = [
                  'maintenance',
                  # Mimics maint performed
                  test_maint_performed[int(self.gen_random_float(0,5))],
                  self.generate_random_date_string(),
                  # Mimics mils maint was performed
                  last_maint_milage + int(self.gen_random_float(5000, 60000))
                  ]
            last_maint_milage = row[3]
         # This generates a gas fillup row
         else:
            row = [
                  self.gen_random_float(0, 50),
                  # Mimics gallons added
                  self.gen_random_float(0, 15),
                  self.generate_random_date_string(),
                  # Mimics mils per tank
                  self.gen_random_float(0, 500)
                  ]
         yield row

   def gen_incomplete_car_data(self):
      complete_data = self.gen_test_car_data()
      incomplete_list = [None, "NA"]
      yield next(complete_data)
      for row in complete_data:
         if self.generate_random_true_false(0.2):
            ind = random.randint(0, len(row) -1)
            row[ind] = random.choice(incomplete_list)
         yield row
