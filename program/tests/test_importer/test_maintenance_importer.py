import unittest
import sqlite3

from ...sql.importer import GenTables, NewImporter
from ...sql.sql_helper import MaintSQL
from .importer_utils import GenImporterTestData as TestData

class TestMaintenanceImporter(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      cls.con = sqlite3.connect(':memory:', detect_types=sqlite3.PARSE_DECLTYPES)
      cls.cur = cls.con.cursor()
      GenTables(con=cls.con).gen_maint()
      cls.TestSQL = MaintSQL(con= cls.con)
      
   @classmethod
   def tearDownClass(cls):
      cls.con.close()

   def setUp(self):
      self.cur.execute(
            "DELETE FROM maint"
            )
      self.test_data = TestData().gen_incomplete_car_data()
      _, _, unsorted_maint = NewImporter().car_sorter(self.test_data)
      self.sorted_maint = NewImporter().maintenance_sorter(unsorted_maint)


   def test_import_with_fresh_table(self):
      NewImporter().maintenance_importer(self.sorted_maint, con=self.con)
      test_data = self.TestSQL.gettable()
      for row in test_data:
         sorted_data = self.sorted_maint.get(row.maint, None)
         stored_data = self.TestSQL.MaintWrite(
               maint= row.maint,
               date= row.date,
               miles= row.miles
               )
         self.assertEqual(stored_data, sorted_data)

   def test_import_with_filled_table(self):
      NewImporter().maintenance_importer(self.sorted_maint)
      new_data = TestData().gen_incomplete_car_data()
      _, _, new_unsorted = NewImporter().car_sorter(new_data)
      new_sorted = NewImporter().maintenance_sorter(new_unsorted)
      NewImporter().maintenance_importer(new_sorted, con=self.con)
      test_data = self.TestSQL.gettable()
      for row in test_data:
         sorted_data = new_sorted.get(row.maint, None)
         stored_data = self.TestSQL.MaintWrite(
               maint= row.maint,
               date= row.date,
               miles= row.miles
               )
         self.assertEqual(stored_data, sorted_data)

   def test_row_id_on_update(self):
      NewImporter().maintenance_importer(self.sorted_maint)
      old_data = self.TestSQL.gettable()
      new_data = TestData().gen_incomplete_car_data()
      _, _, new_unsorted = NewImporter().car_sorter(new_data)
      new_sorted = NewImporter().maintenance_sorter(new_unsorted)
      NewImporter().maintenance_importer(new_sorted, con=self.con)
      for old_row in old_data:
         new_row = self.TestSQL.getrow(search=old_row.rowid)
         self.assertEqual(new_row.rowid, old_row.rowid)
         self.assertEqual(new_row.maint, old_row.maint)

