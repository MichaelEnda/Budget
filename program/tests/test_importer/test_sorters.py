import unittest


from ...sql.importer import MaintSQL, NewImporter
from .importer_utils import GenImporterTestData as TestData



class TestFileSorter(unittest.TestCase):
   def setUp(self):
     self.car_data = TestData().gen_test_car_data()
     
   def test_file_sorter_ufcu_checking(self):
      checking_data = TestData().gen_ufcu_checking_data()
      data_list = [('ufcu', checking_data), ('car', self.car_data)]
      for ftype, data in data_list:
         with self.subTest(msg=ftype):
            test_ftype = NewImporter().file_sorter(data, 'test')[0]
            self.assertEqual(test_ftype, ftype)

   def test_file_sorter_ufcu_saving(self):
      saving_data = TestData().gen_ufcu_saving_data()
      data_list = [('ufcu', saving_data), ('car', self.car_data)]
      for ftype, data in data_list:
         with self.subTest(msg=ftype):
            test_ftype = NewImporter().file_sorter(data, 'test')[0]
            self.assertEqual(test_ftype, ftype)

   def test_file_sorter_ufcu_cc(self):
      cc_data = TestData().gen_ufcu_credit_data()
      data_list = [('ufcu', cc_data), ('car', self.car_data)]
      for ftype, data in data_list:
         with self.subTest(msg=ftype):
            test_ftype = NewImporter().file_sorter(data, 'test')[0]
            self.assertEqual(test_ftype, ftype)


class TestCarSorter(unittest.TestCase):
   def test_maint_gas_seperation(self):
      complete_data = TestData().gen_test_car_data()
      copy = []
      gas_rows = 0
      maint_rows = 0
      # Skips the header of the gas iter
      next(complete_data)
      for row in complete_data:
         if row[0] == 'maintenance':
            maint_rows += 1
         else:
            gas_rows += 1
         # appends each row to a list to generate a new iterable
         copy.append(row)
      _, test_gas, test_maint = NewImporter().car_sorter(iter(copy))
      check_tup = (('gas', gas_rows, test_gas), 
                   ('maint', maint_rows, test_maint))
      for name, data, test in check_tup:
         with self.subTest(msg=name):
            self.assertEqual(data, len(test))
   
   def test_handling_incomplete_rows_gas(self):
      incomplete_data = TestData().gen_incomplete_car_data()
      # This skipps the header row to pass into the sorter
      next(incomplete_data)
      _, test_gas, _ = NewImporter().car_sorter(incomplete_data)
      for row in test_gas:
         with self.subTest(msg=row):
            self.assertNotIn(None, row) 
            self.assertNotIn("NA", row)


class TestUfirstSorter(unittest.TestCase):
   def setUp(self):
      savings_data = TestData().gen_ufcu_saving_data()
      next(savings_data)
      self.savings_data = savings_data
      checking_data = TestData().gen_ufcu_checking_data()
      next(checking_data)
      self.checking_data = checking_data
      cc_data = TestData().gen_ufcu_credit_data()
      next(cc_data)
      self.cc_data = cc_data
      self.all_acts = [self.checking_data, self.savings_data, self.cc_data]

   def test_sorter_change_check(self):
      acts_as_lists = [list(x) for x in self.all_acts]
      for act in acts_as_lists:
         for row in act:
            change_check = NewImporter().ufcu_change_check(row=row)
            if row[4] and row[5]:
               self.assertFalse(expr="Incoming data has expenditure and income")
            elif row[4]:
               change = -float(row[4])
            elif row[5]:
               change = float(row[5])
            else:
               self.assertFalse(expr="Incoming data has neither expend nor income")
            with self.subTest(msg=f"{change_check}, {change}"):
               self.assertEqual(change_check, change)


   def test_ufcu_seperation(self):
      savings_data = TestData().gen_ufcu_saving_data()
      next(savings_data)
      checking_data = TestData().gen_ufcu_checking_data()
      next(checking_data)
      cc_data = TestData().gen_ufcu_credit_data()
      next(cc_data)
      test_list = [
            (
               'UFirst Savings',
               NewImporter().ufcu_sorter(
                  savings_data
                  )[2]
               ),
            (
               'UFirst Checking',
               NewImporter().ufcu_sorter(
                 checking_data 
               )[2]
               ),
            (
               'UFirst Credit Card',
               NewImporter().ufcu_sorter(
                  cc_data
                  )[2]
               )
            ]
      for test in test_list:
         with self.subTest(msg=test[0]):
            self.assertEqual(test[0], test[1])


class TestMaintenanceSorter(unittest.TestCase):
   def test_incoming_data_filter(self):
      _, _, unfiltered_data = NewImporter().car_sorter(
            TestData().gen_test_car_data()
            ) 
      test_data = NewImporter().maintenance_sorter(unfiltered_data)
      for key, item in test_data.items():
         with self.subTest(msg=key):
            for row in unfiltered_data:
               if row[0] == key:
                  unfiltered_row = MaintSQL.MaintWrite(
                        maint= row[0],
                        date= row[1],
                        miles= row[2]
                        )
                  self.assertLessEqual(unfiltered_row.date, item.date)
                  self.assertLessEqual(unfiltered_row.miles, item.miles)
