import unittest
import sqlite3

from .importer_utils import GenImporterTestData as TestData
from ...sql.importer import NewImporter
from ...sql.gen_tables import GenTables
from ...sql.sql_helper import SpendingSQL


class TestUfcuImporter(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      cls.con = sqlite3.connect(':memory:', detect_types=sqlite3.PARSE_DECLTYPES)
      cls.cur = cls.con.cursor()
      GenTables(con=cls.con).gen_spending()
      cls.TestSQL = SpendingSQL(con= cls.con)
      
   @classmethod
   def tearDownClass(cls):
      cls.con.close()

   def setUp(self):
      self.cur.execute(
            "DELETE FROM spending"
            )
      raw_checking  = TestData().gen_ufcu_checking_data()
      next(raw_checking)
      _, self.test_checking, self.check_act = NewImporter().ufcu_sorter(raw_checking)
      raw_saving = TestData().gen_ufcu_saving_data()
      next(raw_saving)
      _, self.test_savings, self.saving_act = NewImporter().ufcu_sorter(raw_saving)
      raw_cc = TestData().gen_ufcu_credit_data()
      next(raw_cc)
      _, self.test_cc, self.cc_act = NewImporter().ufcu_sorter(raw_cc)


   def test_ufcu_checking_data_integrity(self):
      test_checking_list = list(self.test_checking)
      pre_write = [
            self.TestSQL.SpendingWrite(
               acc=self.check_act,
               date=row[1],
               desc=row[3],
               change=NewImporter().ufcu_change_check(row=row),
               bal=row[7]
               ) for row in test_checking_list 
            ]
      NewImporter().ufcu_importer(
                  data= test_checking_list,
                  act = self.check_act,
                  con = self.con
                  )
      table_data = self.TestSQL.gettable()
      post_write = []
      for row in table_data:
         test_row = self.TestSQL.SpendingWrite(
               acc = row.acc,
               date= row.date,
               desc = row.desc,
               change = row.change,
               bal = row.bal
               )
         post_write.append(test_row)
      self.assertEqual(pre_write, post_write)

   def test_ufcu_saving_data_integrity(self):
      test_saving_list = list(self.test_savings)
      pre_write = [
            self.TestSQL.SpendingWrite(
               acc=self.saving_act,
               date=row[1],
               desc=row[3],
               change=NewImporter().ufcu_change_check(row=row),
               bal=row[7]
               ) for row in test_saving_list 
            ]

      NewImporter().ufcu_importer(
                     data= test_saving_list,
                     act = self.saving_act,
                     con = self.con
                     )
      table_data = self.TestSQL.gettable()
      post_write = []
      for row in table_data:
         test_row = self.TestSQL.SpendingWrite(
               acc = row.acc,
               date= row.date,
               desc = row.desc,
               change = row.change,
               bal = row.bal
               )
         post_write.append(test_row)
      self.assertEqual(pre_write, post_write)

   def test_ufcu_cc_data_integrity(self):
      test_cc_list = list(self.test_cc)
      pre_write = [
            self.TestSQL.SpendingWrite(
               acc=self.cc_act,
               date=row[1],
               desc=row[3],
               change=NewImporter().ufcu_change_check(row=row),
               bal=row[7]
               ) for row in test_cc_list 
            ]
      NewImporter().ufcu_importer(
                     data= test_cc_list,
                     act = self.cc_act,
                     con = self.con
                     )
      table_data = self.TestSQL.gettable()
      post_write = []
      for row in table_data:
         test_row = self.TestSQL.SpendingWrite(
               acc = row.acc,
               date= row.date,
               desc = row.desc,
               change = row.change,
               bal = row.bal
               )
         post_write.append(test_row)
      self.assertEqual(pre_write, post_write)


