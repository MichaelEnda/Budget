from dataclasses import astuple
import unittest
import sqlite3

from ...sql.importer import GenTables
from ...sql.sql_helper import HistorySQL 
from ..utils.gen_test_hist import HistoryUtilFuncs as HistUtil

class TestHistorySQL(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      cls.con = sqlite3.connect(':memory:', detect_types=sqlite3.PARSE_DECLTYPES)
      cls.cur = cls.con.cursor()
      GenTables(con=cls.con).gen_history()
      cls.TestSQL = HistorySQL(con= cls.con)

   @classmethod
   def tearDownClass(cls):
      cls.con.close()

   def setUp(self):
      self.cur.execute("DELETE FROM history")

   def test_table_creation(self):
     # name, type, notnull, default value, prmary key, hidden
      expected = [
             ('ID', 'INTEGER', 0, None, 1),
             ('startdate', 'DATE', 1, None, 0),
             ('groceries', 'REAL',1 , None, 0),
             ('gas', 'REAL',1 , None, 0),
             ('eattingout', 'REAL',1 , None, 0),
             ('vices', 'REAL',1 , None, 0),
             ('rent', 'REAL',1 , None, 0),
             ('bill', 'REAL',1 , None, 0),
             ('investing', 'REAL',1 , None, 0),
             ('otherexpend', 'REAL',1 , None, 0),
             ('wages', 'REAL',1 , None, 0),
             ('scholarships', 'REAL',1 , None, 0),
             ('otherincome', 'REAL',1 , None, 0),
             ('netincome', 'REAL',1 , None, 0),
             ('netexpend', 'REAL',1 , None, 0),
             ('savings', 'REAL',1 , None, 0)
             ]

      table_info = self.cur.execute(
            "PRAGMA main.table_info(history)"
                                    ).fetchall()
      test_rows = [(x[1:]) for x in table_info]
      for expect, row in zip(expected, test_rows):
         with self.subTest(msg= row):
            self.assertEqual(expect, row)
      
   def test_add_hist_data_integrity(self):
      before_write = []
      for row in HistUtil().gen_writeable_hist_data():
         before_write.append(row)
         self.TestSQL.addrow(row)
      test_data = self.TestSQL.gettable()
      for before, test in zip(before_write, test_data):
         before_test = astuple(before)
         after_test = astuple(test)[1:]
         self.assertEqual(before_test, after_test)

   def test_get_hist_row_by_id(self):
      before_write = []
      for id, row in enumerate(HistUtil().gen_writeable_hist_data(), start=1):
         before_write.append((id, row))
         self.TestSQL.addrow(row)
      for id, row in before_write:
         print(id, row)
         before_test = astuple(row)
         got_row = self.TestSQL.getrow(rowid=id)
         test_row =astuple(got_row)[1:]
         self.assertEqual(before_test, test_row)
