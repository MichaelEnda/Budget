from random import randint, uniform
from datetime import date, datetime, timedelta
import unittest
import sqlite3



from ...sql.sql_helper import MaintSQL, MaintCheckSQL
from ...sql.gen_tables import GenTables

class Helpers:
   def gen_maint_data(self, n, good:bool= True):
      # Define a list of possible string values
      string_values = ["apple", "banana", "orange", "pear", "grape", "pineapple"]
      date_fmt = ["%Y-%m-%d", "%m/%d/%Y"]
      if not good:
         date_fmt = [    
                    '%d-%b-%Y',
                    '%H:%M:%S',
                    '%I:%M:%S %p',
                    '%a, %d %b %Y %H:%M:%S %Z'
                    ]


      # Loop n times to generate n random lists
      for i in range(n):
         yield_data = []
         for fmt in date_fmt:
          # Generate a random string, date, float, and string
          random_string = string_values[randint(0, len(string_values) - 1)]
          random_date = (datetime.now() - timedelta(days=randint(1, 365))).strftime(fmt)
          random_float = round(uniform(0, 100), 2)
          random_string2 = string_values[randint(0, len(string_values) - 1)]

          # Yield the values as a tuple
          yield_data.append((random_string, random_date, random_float, random_string2))

         yield yield_data

   def gen_write_data(self, n):
      data = []
      for x, _ in self.gen_maint_data(n):
         data.append(x)
      for datum in data:
         yield MaintSQL.MaintWrite(
               maint= datum[0],
               date= datum[1],
               miles= datum[2],
               note= datum[3]
               )

class TestMaint(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      cls.con = sqlite3.connect(':memory:', detect_types=sqlite3.PARSE_DECLTYPES)
      cls.cur = cls.con.cursor()
      GenTables(con=cls.con).gen_maint()
      GenTables(con=cls.con).gen_maint_check()
      
   @classmethod
   def tearDownClass(cls):
      cls.con.close()

   def setUp(self):
      self.cur.execute(
            "DELETE FROM maint"
            )


   def test_maint_table_exists(self):
      db_data = self.cur.execute(
            "SELECT name FROM sqlite_master WHERE type='table'"
            ).fetchall()
      test_data = [x[0] for x in db_data]
      self.assertIn('maint', test_data)

   def test_table_creation(self):
      pragma_data = self.cur.execute(
            "SELECT * FROM PRAGMA_TABLE_INFO('maint')"
            ).fetchall()
      compare_data = [
            #cid|name|type|notnull|dflt_value|pk
            (0,'ID', "INTEGER", 0,None, 1),
            (1,'maint', "TEXT", 1, None, 0),
            (2,'date', "DATE", 1, None, 0),
            (3,'miles', "INT", 1, None, 0),
            (4,'note', "TEXT", 1, None, 0),
            ]
      self.assertEqual(pragma_data, compare_data)

   def test_maint_write_good_date(self):
      good_data = next(Helpers().gen_maint_data(1))
      for datum in good_data:
         with self.subTest(msg=datum[1]):
            test_data = MaintSQL.MaintWrite(
                           maint= datum[0],
                           date= datum[1],
                           miles= datum[2],
                           note= datum[3]
                           )
            self.assertIsInstance(test_data.date, date)

   def test_maint_write_bad_date(self):
      bad_data = next(Helpers().gen_maint_data(1, good=False))
      for datum in bad_data:
         with self.subTest(msg=datum[1]):
            with self.assertRaises(ValueError):
               test_data = {
                     'maint': datum[0],
                     'date': datum[1],
                     'miles': datum[2],
                     'note': datum[3]
                     }
               MaintSQL.MaintWrite(**test_data)

   def test_maint_addrow(self):
     good_data = next(Helpers().gen_maint_data(1))[0]
     test_write = MaintSQL.MaintWrite(
           maint= good_data[0],
           date = good_data[1],
           miles= good_data[2],
           note = good_data[3]
           )
     before_add = self.cur.execute(
           "SELECT * FROM maint"
           ).fetchall()
     MaintSQL(con=self.con).addrow(test_write)
     after_add = self.cur.execute(
           "SELECT * FROM maint"
           ).fetchall()
     self.assertLess(len(before_add), len(after_add))

   def test_succ_getrow_by_id(self):
      write_data = next(Helpers().gen_write_data(1))
      MaintSQL(con=self.con).addrow(write_data)
      test = MaintSQL(con=self.con).getrow(search=1)
      self.assertIsInstance(test, MaintSQL.MaintRead)

   def test_fail_getrow_by_id(self):
      write_data = next(Helpers().gen_write_data(1))
      MaintSQL(con=self.con).addrow(write_data)
      with self.assertRaises(ValueError):
         MaintSQL(con=self.con).getrow(search=2)

   def test_succ_getrow_by_maint(self):
      write_data = next(Helpers().gen_write_data(1))
      MaintSQL(con=self.con).addrow(write_data)
      test = MaintSQL(con=self.con).getrow(search=write_data.maint)
      self.assertIsInstance(test, MaintSQL.MaintRead)

   def test_fail_getrow_by_maint(self):
      write_data = next(Helpers().gen_write_data(1))
      MaintSQL(con=self.con).addrow(write_data)
      with self.assertRaises(ValueError):
         MaintSQL(con=self.con).getrow(search="FAIL")

   def test_updaterow(self):
      good_data = Helpers().gen_write_data(2)
      first_row = next(good_data)
      MaintSQL(con=self.con).addrow(first_row)
      before_update = MaintSQL(con=self.con).getrow(first_row.maint)
      MaintSQL(con=self.con).update_row(
            rowid= before_update.rowid,
            updata=next(good_data)
            )
      after_update = MaintSQL(con=self.con).getrow(before_update.rowid)
      self.assertNotEqual(before_update, after_update)

   def test_maint_gettable(self):
      written_rows = 0
      for row in Helpers().gen_write_data(10):
         try:
            MaintSQL(con=self.con).addrow(row)
         except sqlite3.IntegrityError:
            pass
         else:
            written_rows += 1
      test = MaintSQL(con=self.con).gettable()
      self.assertEqual(len(test), written_rows)

   def test_maint_check_table(self):
      def gen_test_maintcheck():
         GenTables(con=self.con).gen_maint_check()
         test_maint = MaintSQL(con=self.con).gettable()
         for row in test_maint:
            MaintCheckSQL(con=self.con).addrow(
                  MaintCheckSQL.MaintCheckWrite(
                     maint= row.maint,
                     milerecur= 1,
                     daterecur= 1
                     )
                  )

      # This is all setup to test the check table function
      good_data = Helpers().gen_write_data(10)
      for row in good_data:
         try:
            MaintSQL(con=self.con).addrow(row)
         except sqlite3.IntegrityError:
            pass
      gen_test_maintcheck()
      TestCon = MaintSQL(con=self.con)

      #This is where the testing begins
      tests = [
            # Should return an empty dict. Miles and date are too low
            (TestCon.check_table(curmiles=0, curdate=date(year=date.min.year, month=1, day=1)), 'neither'),
            # Should return a non empty dict. Miles pass, date fails
            (TestCon.check_table(curmiles=999999999999, curdate=date(year=date.min.year, month=1, day=1)), 'miles'),
            # Should return a non empty dict. Miles fail, date passes
            (TestCon.check_table(curmiles=0, curdate=date(year=date.max.year, month=1, day=1)), 'date'),
            # Should return a non empty dict. Miles passes, date passes
            (TestCon.check_table(curmiles=999999999999, curdate=date(year=date.max.year, month=1, day=1)), 'both'),
            ]
      for test, name in tests:
         with self.subTest(msg=name):
            if name == 'neither':
               self.assertEqual(len(test), 0)
            else:
               self.assertNotEqual(len(test), 0)

