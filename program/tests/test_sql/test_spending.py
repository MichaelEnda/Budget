from datetime import date
import sqlite3
import unittest

from ...sql.gen_tables import GenTables
from ...sql.sql_helper import SpendingSQL
from ..utils.gen_test_spending import GenTestSpendingData as GenSpending

class TestSpendingSQL(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      cls.con = sqlite3.connect(':memory:', detect_types=sqlite3.PARSE_DECLTYPES)
      cls.cur = cls.con.cursor()
      GenTables(con=cls.con).gen_spending()
      cls.TestSpending = SpendingSQL(con= cls.con)

   @classmethod
   def tearDownClass(cls):
      cls.con.close()

   def setUp(self):
      self.cur.execute("DELETE FROM spending")

   def test_get_balance(self):
      check = {x:(date.min, -9999.0) for x in GenSpending().account_list}
      for row in GenSpending().gen_spending_test_data():
         if row.date > check[row.acc][0]:
            check[row.acc] = (row.date, row.bal)
         self.TestSpending.add_row(row)
      check_savings = round(sum([y[1] for x, y in check.items()]), 2)
      test_data = self.TestSpending.get_savings(datestr="2023-12-31")
      self.assertEqual(check_savings, test_data)


if __name__ == "__main__":
   TestSpendingSQL().test_get_balance()
