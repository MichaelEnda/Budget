from datetime import datetime, timedelta
import random

class GenRawTestData(object):
   def generate_unique_date(self, start_date, end_date, generated_dates):
       # Convert the start and end dates to datetime objects
       start_datetime = datetime.strptime(start_date, '%Y-%m-%d')
       end_datetime = datetime.strptime(end_date, '%Y-%m-%d')

       while True:
           # Generate a random date between the start and end dates
           random_date = start_datetime + timedelta(days=random.randint(0, (end_datetime - start_datetime).days))

           # Format the random date as a string
           random_date_string = random_date.strftime('%Y-%m-%d')

           # Check if the generated date is already in the list of generated dates
           if random_date_string not in generated_dates:
               # If the date is unique, return it
               return random_date_string

   def gen_random_float(self, low: float, high: float) -> float:
      return round(random.uniform(low, high), 2)

