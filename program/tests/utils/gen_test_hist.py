import random
from datetime import datetime, timedelta
from collections.abc import Iterator
from typing import Iterable



from ...sql.sql_helper import HistorySQL
from .importer_utils import GenImporterTestData as Gen

class HistoryUtilFuncs:
   def generate_unique_date(self, start_date, end_date, generated_dates):
       # Convert the start and end dates to datetime objects
       start_datetime = datetime.strptime(start_date, '%Y-%m-%d')
       end_datetime = datetime.strptime(end_date, '%Y-%m-%d')

       while True:
           # Generate a random date between the start and end dates
           random_date = start_datetime + timedelta(days=random.randint(0, (end_datetime - start_datetime).days))

           # Format the random date as a string
           random_date_string = random_date.strftime('%Y-%m-%d')

           # Check if the generated date is already in the list of generated dates
           if random_date_string not in generated_dates:
               # If the date is unique, return it
               return random_date_string

   def _gen_hist_row(self, used_dates: list[str]) -> list:
      G = Gen()
      row = [self.generate_unique_date(start_date="2023-01-01",
                                       end_date="2023-12-31",
                                       generated_dates=used_dates)]
      [row.append(G.gen_random_float(0,1000)) for x in range(0,12)]
      return row

   def gen_raw_hist_data(self) -> Iterable[list]:
      used = []
      for x in range(100):
         row = self._gen_hist_row(used)
         used.append(row[0])
         yield row

   def gen_writeable_hist_data(self) -> Iterable[HistorySQL.HistoryWrite]:
      used = []
      for x in range(100):
         row = self._gen_hist_row(used)
         used.append(row[0])
         yield HistorySQL.HistoryWrite(*row)


