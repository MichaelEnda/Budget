import random

from ...sql.sql_helper import SpendingSQL
from .gen_raw_data import GenRawTestData as Gen
from ...utils import UseDates

class GenTestSpendingData(object):
   @property
   def account_list(self):
      return ['UFirst Savings', 'UFirst Checking', 'UFirst Credit Card']

   def _gen_spending_row_test_data(self, account: str, 
                                   used_dates: list[str], lastbal: float=0):
      change = Gen().gen_random_float(-1000, 1000)
      row = SpendingSQL.SpendingWrite(
               acc=account,
               date= Gen().generate_unique_date(start_date="2023-01-01",
                                              end_date="2023-12-31",
                                              generated_dates=used_dates),
               desc= "test",
               change = change,
               bal= lastbal + change
            )
      return row

   def gen_spending_test_data(self):
      account_bal = {x: 0.0 for x in self.account_list}
      old_dates = {x:[UseDates().stdatestr] for x in self.account_list}
      for x in range(100):
         acc = random.choice(self.account_list)
         row = self._gen_spending_row_test_data(account= acc,
                                                used_dates=old_dates.get(acc, ["None"]),
                                                lastbal=account_bal[acc])
         new_bal = account_bal[acc] + row.change
         account_bal[acc] = new_bal
         cur_dates = old_dates[acc]
         cur_dates.append(str(row.date))
         old_dates.update({acc: cur_dates})
         yield row
