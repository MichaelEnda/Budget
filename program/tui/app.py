from textual.app import App, ComposeResult
from textual.reactive import var
from textual.widgets import TabPane, Header, Footer


from .content_mgr import ContentMgr
from .spending_table import SpendingTable
from .one_time import Onetime
from .recurring import Recurring
from .maint import MaintCheck
from .confirm import Confirm


class BudgetApp(App[dict[str, float]]):
    CSS = """
        #buttons {
                margin-top: 1;
                height: 3;
                width: auto;
                }

        TabPane {
                height: 1fr;
                }

        TabbedContent {
                height: 1fr;
                }

        ContentSwitcher {
                height: 1fr;
                }

        DataTable {
                height: 1fr;
                }
        """

    passed_spending = var(dict)

    def compose(self) -> ComposeResult:
        yield Header()
        with ContentMgr(initial='data-table'):
            with TabPane("Spending", id='data-table'):
                yield SpendingTable( classes="home")
            with TabPane("OneTime",id='onetime'):
                yield Onetime( initial='viewonetime', classes="home")
            with TabPane("Recurring", id="recurring"):
                yield Recurring(initial='viewrecurring', classes="home")
            with TabPane("Maintenance", id="maint"):
                yield MaintCheck(initial='entermiles', classes="home")
#            with TabPane("Confirm", id='conf'):
#                yield Confirm(classes='home')
        yield Footer()

    def on_spending_table_pass_spending(self, msg:SpendingTable.PassSpending):
        msg.stop()
        self.passed_spending = msg.pass_spending

    def on_content_mgr_done(self, msg:ContentMgr.Done):
        msg.stop()
        self.exit(self.passed_spending)

if __name__ == "__main__":
    app = BudgetApp()
    app.run()
