
from textual.app import ComposeResult
from textual.containers import Horizontal
from textual.reactive import reactive, var
from textual.widget import Widget
from textual.widgets import Button, DataTable, Label


class Confirm(Widget):
    passed_spending = reactive(dict)

    def compose(self) -> ComposeResult:
        with Horizontal():
            yield DataTable(id='passed_spending')
        with Horizontal():
            yield Label("test2")
        with Horizontal():
            yield Button(label="Done")

    def on_mount(self):
        dt_rows = []
        for key, value in self.passed_spending.items():
            dt_rows.append((key, value))
        print('dt_rows', dt_rows)
        self.query_one(DataTable).add_rows(dt_rows)

    def on_focus(self):
        print("I'm FOCUSED!")
