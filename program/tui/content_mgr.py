from textual.binding import Binding
from textual.message import Message
from textual.widget import Widget
from textual.widgets import ContentSwitcher, TabPane, TabbedContent, Tabs


class ContentMgr(TabbedContent):

    BINDINGS = [
            Binding(']', 'next_tab', 'go to next tab', priority=True)
            ]


    class NextTab(Message):
        def __init__(self) -> None:
            super().__init__()

    class Done(Message):
        def __init__(self) -> None:
            super().__init__()

    def action_next_tab(self):
        if self.query_one(Tabs).active == "maint":
            self.post_message(self.Done())
        self.query_one(Tabs).action_next_tab()
        active_tab = self.query_one(Tabs).active
        self.active = active_tab
        self.get_child_by_type(ContentSwitcher).current = active_tab
        # Above sets the correct active/current for the tabs/content switcher
        # This links the tab ids and child widgets key = tab id, value = child widget
        linked = {tab.id: content for tab, content in zip(self.query(TabPane), self.query('.home'))}
        test = linked.get(active_tab)
        assert isinstance(test, Widget)
        
        # Walk children gives all possible child widgets
        test = test.walk_children()
        for child in test:
            assert isinstance(child, Widget)
            if child.focusable:
                child.focus()

    def on_mount(self):
        self.focus()

    def on_tabs_tab_activated(self, msg:Tabs.TabActivated):
        msg.stop()










