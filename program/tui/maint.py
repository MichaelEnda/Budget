from datetime import timedelta, date
from typing import Tuple

from textual.app import ComposeResult
from textual.binding import Binding
from textual.containers import Horizontal, Vertical
from textual.coordinate import Coordinate
from textual.widget import Widget
from textual.widgets import  DataTable, ListView, ListItem, Label, Input, ContentSwitcher, Button
from textual.reactive import  reactive, var
from textual.message import Message

from ..sql import MilesSQL, MaintSQL, MaintCheckSQL
from ..utils.helper import UseDates

class ViewMaintCheck(DataTable):
    curmiles = var(0)
    cursor_type = "row"
    BINDINGS = [
        Binding('j', 'cursor_down', 'moves cursor down', show=False),
        Binding('k', 'cursor_up', 'moves cursor up', show=False),
        Binding('l', 'cursor_right', 'moves cursor right', show=False),
        Binding('h', 'cursor_left', 'moves cursor left', show=False),
        Binding('e', 'edit_maint', 'Edit Maintenance Data'),
        Binding(']', 'maint_done', 'Finish editing Maintenance')
        ]

    class EditMaint(Message):
        def __init__(self, sel_maint: str) -> None:
            self.sel_maint = sel_maint
            super().__init__()

    class MaintDone(Message):
        def __init__(self) -> None:
            super().__init__()

    def gen_rows(self):
        maint_data = {x.maint: x for x in MaintSQL().gettable()}
        needs_maint = MaintCheckSQL().check_table(
            curmiles = self.curmiles,
            curdate = UseDates.enddate
            )
        for maint, data in maint_data.items():
            row = []
            # Checks if the maintenance needs done, if so adds an X
            if maint in needs_maint.keys():
                row.append("X")
            else:
                row.append("")
            # adds the name of the maintenance
            row.append(maint)
            # adds the date last performed
            row.append(data.date)
            # Adds the miles last performed
            row.append(data.miles)
            maintcheck_data = MaintCheckSQL().getrow(search=maint)
            if maintcheck_data:
                row.append(data.date + timedelta(maintcheck_data.daterecur))
                row.append(data.miles + maintcheck_data.milerecur)
            else:
                row[0] = "No Check Data"
                row.append("No Check Data")
                row.append("No Check Data")
            row.append(data.note)
            yield row

    def gen_table(self):
        self.clear()
        for row in self.gen_rows():
            self.add_row(*row)

    def on_mount(self):
        headers = [
                "Needs Done", 
                "Maintenance", 
                "Last Date", 
                "Last Miles",
                "Next Date",
                "Next Miles",
                "Notes"
                ]

        self.add_columns(*headers)
        self.gen_table()

    def action_edit_maint(self):
        cur_coord = Coordinate(column=1, row=self.cursor_row)
        if self.is_valid_coordinate(cur_coord):
            sel_maint = self.get_cell_at(cur_coord)
            self.post_message(
                    self.EditMaint(sel_maint)
                    )

    def action_maint_done(self):
        # Used in app.py to exit the app
        self.post_message(ViewMaintCheck.MaintDone())

class EnterMiles(Widget):
    def compose(self) -> ComposeResult:
        yield Label("Please enter current Milage: ")
        yield Input(placeholder="Milage as an float", id="in_miles")


class EditMaintCheck(Widget):
    sel_maint = var(str)
    maintrowid = var(int)
    maintcheckrowid = var(int)

    BINDINGS = [
            Binding("escape", 'go_back', "Return to View Maintenance")
            ]

    class DoneEditMaintCheck(Message):
        def __init__(self) -> None:
            self.maint_suc = False
            self.check_suc = False
            super().__init__()

    def gen_maint_inputs(self):
        maint_data = MaintSQL().getrow(search=self.sel_maint)
        # Date
        yield Label("Date Last Performed")
        yield Input(value=date.strftime(maint_data.date, "%Y-%m-%d"), 
                    id="date",
                    classes="maint")
        # Miles
        yield Label("Miles Last Performed")
        yield Input(value=str(maint_data.miles), 
                    id="miles", 
                    classes="maint")
        # Note
        yield Label("Note")
        yield Input(value=maint_data.note, id="note", classes="maint")

    def gen_check_inputs(self):
        check_data = MaintCheckSQL().getrow(self.sel_maint)
        compose_list = []
        # Daterecur
        compose_list.append(Label("Day Recurrence"))
        compose_list.append(
                Input(
                placeholder="Days of recurrence for the maintenance",
                id="daterecur",
                classes="check"
                    )
                )
        # Mile recur
        compose_list.append(Label("Mile Reccurence"))
        compose_list.append(
                Input(
                placeholder="Miles of recurrence for the maintenance",
                id="milerecur",
                classes="check"
                )
                )
        # This pulls the daterecur and milerecur from compose list to set a value if they exist in the database
        if check_data:
            compose_list[1].value = str(check_data.daterecur)
            compose_list[3].value = str(check_data.milerecur)
        for widget in compose_list:
            yield widget
        
    def compose(self) -> ComposeResult:
        yield Label(self.sel_maint)
        for maint_input in self.gen_maint_inputs():
            yield maint_input
        for check_input in self.gen_check_inputs():
            yield check_input
        # Sets the value of daterecur and mile recur only if the row exists in maintcheck table

        with Horizontal():
            yield Button(label="Done", id='submit')
            yield Label(id="errlabel")

    def on_mount(self):
        self.query(Input).first().focus()

    def on_input_submitted(self, msg: Input.Submitted):
        msg.stop()
        self.screen.focus_next()

    def validate_maint_data(self) -> Tuple[bool, MaintSQL.MaintWrite]:
        maint_values = self.query(Input).filter(".maint")
        validate_maint = MaintSQL().MaintWrite(
                maint=self.sel_maint,
                date=maint_values.filter("#date").only_one(Input).value,
                miles=maint_values.filter("#miles").only_one(Input).value,
                note=maint_values.filter("#note").only_one(Input).value
                )
        return validate_maint.validate(), validate_maint

    def validate_check_data(self) -> Tuple[bool, MaintCheckSQL.MaintCheckWrite]:
        check_values = self.query(Input).filter(".check")
        validate_check = MaintCheckSQL().MaintCheckWrite(
                maint= self.sel_maint,
                daterecur= check_values.filter("#daterecur").only_one(Input).value,
                milerecur=check_values.filter("#milerecur").only_one(Input).value
                )
        return validate_check.validate(), validate_check

    def on_button_pressed(self, msg:Button.Pressed):
        msg.stop()
        maint_is_valid, maint_data = self.validate_maint_data()
        check_is_valid, check_data = self.validate_check_data()
        send_message = self.DoneEditMaintCheck()
        if maint_is_valid and check_is_valid:
            MaintSQL().add_or_update(maint_data)
            MaintCheckSQL().add_or_update(check_data)
            send_message.maint_suc = True
            send_message.check_suc = True
        elif not check_is_valid and maint_is_valid:
            send_message.check_suc = False
            send_message.maint_suc = True
        elif not maint_is_valid and check_is_valid:
            send_message.maint_suc = False
            send_message.check_suc = True
        # This message is handled in MaintCheck
        self.post_message(send_message)

    def action_go_back(self):
        # I can send this message back safely as all the database writing
        # happens in on_button_pressed so this will just go back to ViewMaintCheck
        back_msg = self.DoneEditMaintCheck()
        back_msg.check_suc = True
        back_msg.maint_suc = True
        self.post_message(
                back_msg
                )
        
class MaintCheck(ContentSwitcher):
    def compose(self) -> ComposeResult:
        yield EnterMiles(id='entermiles')
        yield ViewMaintCheck(id='viewmaintcheck')

    def on_mount(self):
        self.check_cur_miles()

    def check_cur_miles(self):
        curmiles = MilesSQL().curmiles
        if curmiles:
            self.query_one(EnterMiles).query_one("#in_miles", Input).value = str(curmiles)

    def on_input_submitted(self, msg:Input.Submitted) -> None:
        msg.stop()
        if msg.input.id == 'in_miles':
            self.validate_miles(msg=msg)

    def validate_miles(self, msg:Input.Submitted) -> None:
        try:
            curmiles = int(msg.value)
        except Exception:
            in_miles = self.query_one('#in_miles', Input)
            in_miles.cursor_position = 0
            in_miles.action_delete_right_all()
            in_miles.placeholder = "Please enter a valid milage"
        else:
            view_maint = self.query_one(ViewMaintCheck)
            view_maint.curmiles = curmiles
            MilesSQL().add_or_update(data=curmiles)
            self.current = view_maint.id
            view_maint.focus()

    def on_view_maint_check_edit_maint(self, msg:ViewMaintCheck.EditMaint):
        msg.stop()
        edit_maint = EditMaintCheck(id='editmaintcheck')
        edit_maint.sel_maint = msg.sel_maint
        self.mount(edit_maint)
        self.current = edit_maint.id
        edit_maint.focus()

    def on_edit_maint_check_done_edit_maint_check(self, 
                                                msg:EditMaintCheck.DoneEditMaintCheck):
        msg.stop()
        view_maint = self.query_one(ViewMaintCheck)
        edit_maint = self.query_one(EditMaintCheck)
        error_label = edit_maint.query_one("#errlabel", Label)
        if msg.maint_suc and msg.check_suc:
            view_maint.gen_table()
            self.current = view_maint.id
            view_maint.focus()
            edit_maint.remove()
        elif not msg.maint_suc and not msg.check_suc:
            error_label.update("Please double check all inputs.")
        elif not msg.maint_suc:
            error_label.update(
                    "Please double check input for: miles, and date"
                    )
        elif not msg.check_suc:
            error_label.update(
                    "Please double check input for: daterecur and milerecur"
                    )
