from dataclasses import astuple
from textual.coordinate import Coordinate
from textual.widgets import DataTable, ListView, ListItem, Label, Input, Button, ContentSwitcher
from textual.containers import Horizontal
from textual.reactive import reactive, var
from textual.binding import Binding
from textual.message import Message
from textual.app import ComposeResult
from textual.css.query import NoMatches

from ..sql import OneTimeSQL

# TODO: finish passing the hightlighted edit row to the edit class.
# Firgure out how to update the database upon returning - use the add_row function of datatable after writing it toeh  the database.
class ViewOneTime(DataTable):
    index = reactive(0)
    cursor_type = "row"

    BINDINGS = [
        Binding('e', 'edit', "Edits current selected onetime"),
        Binding('n', 'new', 'Adds a new plan to database'),
        Binding('d', 'delete', 'Deletes current onetime'),
        Binding('j', 'cursor_down', 'moves cursor down', show=False),
        Binding('k', 'cursor_up', 'moves cursor up', show=False),
        Binding('l', 'cursor_right', 'moves cursor right', show=False),
        Binding('h', 'cursor_left', 'moves cursor left', show=False),
            ]

    class Edit(Message):
        def __init__(self, sqlid: int | None = None) -> None:
            self.sqlid = sqlid
            super().__init__()

    def draw_table(self):
        onetime_data = OneTimeSQL().gettable()
        rows = [astuple(x) for x in onetime_data]
        self.clear()
        self.add_rows(rows)

    def on_mount(self):
        # I seperated the headers as they do not get cleared by self.clear
        headers = OneTimeSQL().get_headers()
        headers = [ str(x) for x in headers ]
        self.add_columns(*headers)
        self.draw_table()

    def action_edit(self):
        # This gives the sql rowid of the highlighted row.
        cur_coord = Coordinate(column=0, row=self.cursor_row)
        if self.is_valid_coordinate(cur_coord):
            sql_row_id = self.get_cell_at(cur_coord)
            self.post_message(
                    self.Edit(sql_row_id)
                    )

    def action_new(self):
        self.post_message(self.Edit(sqlid=None))

    def action_delete(self):
        cur_coord = Coordinate(column=0, row=self.cursor_row)
        if self.is_valid_coordinate(cur_coord):
            sql_row_id = self.get_cell_at(cur_coord)
            OneTimeSQL().removerow(rowid=sql_row_id)
            self.draw_table()



class EditOneTime(ListView):
    index = reactive(0)
    edit_row = var(OneTimeSQL.OneTimeWrite())
    sqlid: reactive[int | None]
    write_list = reactive([ 
                ['Goal', 'What is the goal?', 'goal'],
                ['Cost-fmt: float', 'How much will it cost?', 'cost'],
                ['Date-fmt: YYYY-MM-DD', 'Purchase date?', 'date'],
                         ])
    BINDINGS = [
            Binding('escape', 'back', 'go back to View One Time')
            ]

    class Finished(Message):
        def __init__(self, success: bool) -> None:
            self.success = success
            super().__init__()


    def action_select_cursor(self):
        # focuses the child input of whichever ListItem is selected
        # If no input child focuses the finished button
        sel_item = self.query(ListItem)[self.index]
        try:
            sel_item.get_child_by_type(Input).focus()
        except NoMatches: 
            self.query_one(Horizontal).get_child_by_type(Button).focus()

    def on_input_submitted(self, sub: Input.Submitted):
    # Runs when an input field is submitted
        def add_new_attr(input=sub) -> None:
            if input.input.has_parent:
                check_id = input.input.parent.id
            else: raise ValueError('Input has no parent ID')
            check_val = input.input.value
            if check_id == 'id0': 
                self.edit_row.goal = check_val
            if check_id == 'id1': 
                self.edit_row.cost = check_val
            if check_id == 'id2': 
                self.edit_row.date = check_val
        sub.stop()
        add_new_attr()
        if self.index <= 1:
            self.index = self.index + 1
            self.action_select_cursor()
        else:
            self.query_one(Horizontal).query_one(Button).focus()

    # Runs when the final button is submitted
    def on_button_pressed(self, event:Button.Pressed):
        if self.edit_row.validate():
            OneTimeSQL().add_or_update(self.edit_row)
            self.post_message(self.Finished(success=True))
        else:
            self.post_message(self.Finished(success=False))
        event.stop()

    def action_back(self) -> None:
        self.post_message(self.Finished(success=True))

    def compose(self) -> ComposeResult:
        # Need to figure out a better way to couple the list items to the data they are entering
        if self.sqlid:
            ind = 0
            for list_item in self.write_list:
                 yield ListItem(
                        Label(list_item[0]),
                        Input(value=list_item[1]),
                        id=f"id{ind}",
                        )
                 ind += 1
            with Horizontal():
                yield Button('Finished', id='finished') 
                yield Label(id = 'errorlabel')
        else:
            ind = 0
            for compoment in self.write_list:
                yield ListItem(
                        Label(compoment[0]), 
                        Input(
                            placeholder=compoment[1],
                            ),
                        id=f"id{ind}"
                        )
                ind += 1
            with Horizontal():
                yield Button('Finished', id='finished') 
                yield Label(id="errlabel")

# Figure out how to deal with the the EditOneTime widget resetting
class Onetime(ContentSwitcher):
    def compose(self) -> ComposeResult:
        yield ViewOneTime(id='viewonetime')
        # yield EditOneTime(id='editonetime', disabled=True)

    def one_time_switch_tabs(self, next: str) -> None:
        self.prevent()
        self.current = next
        self.query_one(f'#{next}').focus()

    def on_view_one_time_edit(self, msg: ViewOneTime.Edit) -> None:
        msg.stop()
        self.mount(EditOneTime(id='editonetime'))
        self.query_one(EditOneTime).sqlid = msg.sqlid
        if msg.sqlid:
            sql_data = OneTimeSQL().getrow(msg.sqlid)
            self.query_one(EditOneTime).write_list = [
                    ['Goal', f'{sql_data.goal}'],
                    ['Cost-fmt: float', f"{sql_data.cost}"],
                    ['Date-fmt: YYY-MM-DD', f"{str(sql_data.date)}"]
                    ]
        self.one_time_switch_tabs('editonetime')
        
    def on_edit_one_time_finished(self, msg:EditOneTime.Finished):
        if msg.success:
            self.query_one(ViewOneTime).draw_table()
            self.one_time_switch_tabs('viewonetime')
            self.query_one(EditOneTime).remove()
        else:
            errlabel= self.query_one('#editonetime'
                           ).query_one(Horizontal).query_one("#errlabel", Label)
            errlabel.update("Please enter a valid onetime entry")
            self.query_one(EditOneTime).index = 0
        msg.stop()





