from dataclasses import astuple
from textual.app import ComposeResult
from textual.containers import Horizontal
from textual.coordinate import Coordinate
from textual.widgets import DataTable, ListView, ListItem, Label, Input,Switch,ContentSwitcher, Button
from textual.reactive import  reactive, var
from textual.message import Message
from textual.binding import Binding

from ..sql import RecurringSQL

class ViewRecurring(DataTable):
    index = reactive(0)
    cursor_type = "row"
    rec_sql = RecurringSQL()

    BINDINGS = [
        Binding('e', 'edit', "Edits current selected onetime"),
        Binding('n', 'new', 'Adds a new plan to database'),
        Binding('d', 'delete', 'Deletes current onetime'),
        Binding('j', 'cursor_down', 'moves cursor down', show=False),
        Binding('k', 'cursor_up', 'moves cursor up', show=False),
        Binding('l', 'cursor_right', 'moves cursor right', show=False),
        Binding('h', 'cursor_left', 'moves cursor left', show=False),
            ]

    class Edit(Message):
        def __init__(self, sqlid: int | None = None) -> None:
            self.sqlid = sqlid
            super().__init__()

    def draw_table(self):
        recur_data = RecurringSQL().gettable()
        rows = [astuple(x) for x in recur_data]
        self.clear()
        self.add_rows(rows)

    def on_mount(self):
        self.rec_sql.update_date()
        headers = RecurringSQL().get_headers()
        headers = [ str(x) for x in headers ]
        self.add_columns(*headers)
        self.draw_table()



    def action_edit(self):
        cur_coord = Coordinate(column=0, row=self.cursor_row)
        if self.is_valid_coordinate(cur_coord):
            sql_row_id = self.get_cell_at(cur_coord)
            self.post_message(
                    self.Edit(sql_row_id)
                    )

    def action_new(self):
        self.post_message(self.Edit(sqlid=None))

    def action_delete(self):
        cur_coord = Coordinate(column=0, row=self.cursor_row)
        if self.is_valid_coordinate(cur_coord):
            sql_row_id = self.get_cell_at(cur_coord)
            RecurringSQL().removerow(rowid=sql_row_id)
            self.draw_table()


class EditRecurring(ListView):
    index = reactive(0)
    edit_row = var(RecurringSQL.RecurringWrite())
    sqlid: reactive[int | None]
    write_list = reactive([ 
                ['Goal', 'What is the goal?',],
                ['Cost-fmt: float', 'How much will it cost?'],
                ['Date-fmt: YYYY-MM-DD', 'When is the next payment due?'],
                ['Period-fmt: int', 'What is the recursion period of the payment (months/days)']
                         ])
    switch_default = reactive(True)
    BINDINGS = [
            Binding('escape', 'back', 'go back to View Recurring')
            ]

    class Finished(Message):
        def __init__(self, success: bool) -> None:
            self.success = success
            super().__init__()


    def action_back(self) -> None:
        self.post_message(self.Finished(success=True))

    def on_list_view_selected(self, msg:ListView.Selected):
        msg.item.query(".writevalue").first().focus()

    def on_list_view_highlighted(self, msg:ListView.Highlighted):
        msg.item.query(".writevalue").first().focus()

    def on_input_submitted(self, sub: Input.Submitted):
    # Runs when an input field is submitted
        def add_new_attr(input=sub) -> None:
            if input.input.has_parent:
                check_id = input.input.parent.id
            else: raise ValueError('Input has no parent ID')
            check_val = input.input.value
            if check_id == 'id0': 
                self.edit_row.goal = check_val
            if check_id == 'id1': 
                self.edit_row.cost = check_val
            if check_id == 'id2': 
                self.edit_row.date = check_val
            if check_id == 'id3':
                self.edit_row.period = check_val
        add_new_attr(input=sub)
        sub.stop()
        self.focus()
        if self.validate_index(self.index + 1):
            self.index += 1

    # Runs when the final button is submitted
    def on_button_pressed(self, event:Button.Pressed):
        self.edit_row.month = self.query_one(Switch).value
        is_valid = self.edit_row.validate()
        if is_valid:
            RecurringSQL().add_or_update(self.edit_row)
            self.post_message(self.Finished(success=True))
        else:
            self.post_message(self.Finished(success=False))
        event.stop()

    def compose(self) -> ComposeResult:
        if self.sqlid:
            ind = 0
            for list_item in self.write_list:
                 yield ListItem(
                        Label(list_item[0]),
                        Input(value=list_item[1],
                              classes="writevalue"),
                        id=f"id{ind}",
                        )
                 ind += 1
        else:
            ind = 0
            for compoment in self.write_list:
                yield ListItem(
                        Label(compoment[0]), 
                        Input(
                            placeholder=compoment[1], classes="writevalue",
                            ),
                        id=f"id{ind}"
                        )
                ind += 1
        yield ListItem(
                Label("Period Unit: Day-Month"),
                Switch(value=self.switch_default, 
                       classes="writevalue", 
                       id='monthswitch'),
                id=f"id{ind}"
                )
        ind += 1
        with ListItem():
            with Horizontal(id=f'id{ind}'):
                yield Button('Finished', id='finished', classes="writevalue") 
                yield Label(id='errlabel')


class Recurring(ContentSwitcher):
    def compose(self) -> ComposeResult:
        yield ViewRecurring(id='viewrecurring')

    def recurring_switch_tabs(self, next: str) -> None:
        self.prevent()
        self.current = next
        self.query_one(f'#{next}').focus()

    def on_view_recurring_edit(self, msg: ViewRecurring.Edit) -> None:
        msg.stop()
        self.mount(EditRecurring(id='editrecurring'))
        self.query_one(EditRecurring).sqlid = msg.sqlid
        if msg.sqlid:
            sql_data = RecurringSQL().getrow(msg.sqlid)
            self.query_one(EditRecurring).write_list = [
                    ['Goal', f'{sql_data.goal}'],
                    ['Cost-fmt: float', f"{sql_data.cost}"],
                    ['Date-fmt: YYY-MM-DD', f"{str(sql_data.date)}"],
                    ['Period-fmt: int', f'{str(sql_data.period)}'],
                    ]
            self.query_one(EditRecurring).switch_default = sql_data.month
        self.recurring_switch_tabs('editrecurring')
        
    def on_edit_recurring_finished(self, msg:EditRecurring.Finished):
        if msg.success:
            self.query_one(ViewRecurring).draw_table()
            self.recurring_switch_tabs('viewrecurring')
            self.query_one(EditRecurring).remove()
        else:
            errlabel= self.query_one('#editrecurring'
                           ).query_one(Horizontal).query_one(Label)
            errlabel.update("Please ensure all data is valid and submitted.")
            self.query_one(EditRecurring).index = 0
        msg.stop()


