from dataclasses import astuple
from textual.message import Message
from textual.reactive import var
from textual.widgets import DataTable
from textual.binding import Binding

from ..sql import HistorySQL, SpendingSQL
from ..utils import UseDates
from ..utils.taglist import taglist


class SpendingTable(DataTable):
    ### TODO ###
    # Generate a dictionary for all the different type of expenditures that I have
    # When I move onto the next tab write all of the flagged lines to the @staticmethod
    # Return that dict to the app
    cursor_type = "row"
    pass_spending = var(dict)
    taglist = taglist

    BINDINGS = [
        Binding('j', 'cursor_down', 'moves cursor down', show=False),
        Binding('k', 'cursor_up', 'moves cursor up', show=False),
        Binding('l', 'cursor_right', 'moves cursor right', show=False),
        Binding('h', 'cursor_left', 'moves cursor left', show=False),
        *[Binding(x[0], f'flag_line("{x[1]}")', x[2]) for x in taglist]
    ]


    class PassSpending(Message):
        def __init__(self, spending_dict: dict) -> None:
            self.pass_spending = spending_dict
            super().__init__()

    def action_flag_line(self, flag: str) -> None:
        self.prevent()
        cursor_cord = self.cursor_coordinate
        while cursor_cord.column > 0:
            cursor_cord = cursor_cord.left()
        row_key, col_key  = self.coordinate_to_cell_key(cursor_cord)
        self.update_cell(
                column_key=col_key,
                row_key=row_key,
                value=flag
                )
        # get change row
        sel_cost = self.get_row(row_key=row_key)[4]
        # get current total of category
        cur = self.pass_spending.get(flag, None)
        self.pass_spending.update({flag: cur + sel_cost})

    def gen_pass(self):
        for tag in self.taglist:
            self.pass_spending.update({tag[1]: float(0.0)})

    def gen_table(self):
        headers = ['Tag', 'Account', "Date", "Desc", "Change", "Bal"]
        self.add_columns(*headers)
        indata = SpendingSQL().spending_table_tui(
                st_date=UseDates.stdate,
                end_date=UseDates.enddate
                )
        for row in indata:
            if row.tag:
                cur = self.pass_spending.get(row.tag)
                self.pass_spending.update({row.tag: cur + row.change})
            self.add_row(
                    *(row.tag, row.acc, row.date, row.desc[:50], row.change, row.bal)
                    )

    def on_mount(self) -> None:
        self.gen_pass()
        print(self.BINDINGS)
        self.gen_table()
        self.focus()

    def write_history_with_sorted_spending(self, spending:dict[str, float]):
        hist_row = HistorySQL.HistoryWrite(
            stdate = UseDates.stdate,
            groceries = spending['Gr'],
            gas = spending['Ga'],
            eattingout = spending['E'],
            vices = spending['V'],
            rent = spending['R'],
            bill = spending['B'],
            investing = spending['I'],
            otherexpend = spending['X'],
            wages = spending['W'],
            scholarships = spending['S'],
            otherinc = spending['OI'],
            _payback = spending["P"]
        )
        hist_row.calculate_net()
        HistorySQL().add_or_update(data=hist_row)

    def add_tagging_to_table(self):
        updata = []
        for key in self.rows.keys():
            row_data = self.get_row(key)
            desc = row_data.pop(3)
            tag = row_data.pop(0)
            updata.append((tag, *row_data))
        SpendingSQL().update_rows(updata)

    def on_blur(self):
        self.add_tagging_to_table()
        self.write_history_with_sorted_spending(spending=self.pass_spending)
        self.post_message(
                self.PassSpending(self.pass_spending)
                )
