from datetime import date
from datetime import datetime
from textual.dom import DOMNode
from textual.widget import Widget
from textual.widgets import Input


def val_str(data: str | None, id: str | None, parnode: DOMNode | None) -> str | None:
        try:
            if data: str(data) 
            else: raise ValueError
        except ValueError:
            failed = parnode.query_one(f'#{id}', Input)
            failed.cursor_position = 0
            failed.action_delete_right_all()
            failed.placeholder = "Please enter a valid string"
            failed.focus()
        else:
            return str(data)


def val_int(data: str | None, id: str | None, parnode: DOMNode | None) -> int | None:
        try:
            if data: int(data) 
            else: raise ValueError
        except ValueError:
            failed = parnode.query_one(f'#{id}', Input)
            failed.cursor_position = 0
            failed.action_delete_right_all()
            failed.placeholder = "Please enter a valid integer"
            failed.focus()
        else:
            return int(data)

def val_float(data: str | None, id: str | None, parnode: DOMNode | None) -> float | None:
        try:
            if data: float(data) 
            else: raise ValueError
        except ValueError:
            failed = parnode.query_one(f'#{id}', Input)
            failed.cursor_position = 0
            failed.action_delete_right_all()
            failed.placeholder = "Please enter a valid float"
            failed.focus()
        else:
            return float(data)

def val_date(data: str | None, id: str | None, parnode: DOMNode | None) -> date | None:
        try:
            if data: datetime.strptime(data, "%Y-%m-%d") 
            else: raise ValueError
        except ValueError:
            failed = parnode.query_one(f'#{id}', Input)
            failed.cursor_position = 0
            failed.action_delete_right_all()
            failed.placeholder = "Please enter a valid date: YYYY-MM-DD"
            failed.focus()
        else:
            return datetime.strptime(data, "%Y-%m-%d").date()
