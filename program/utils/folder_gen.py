import os

def create_folders(path):
    folders = path.split('/')

    if folders[0] == "":
        folders = folders[1:]

    
    curpath = ''
    for folder in folders:
        curpath = os.path.join(curpath, folder)
        if not os.path.exists(curpath):
            os.makedirs(curpath)
    
