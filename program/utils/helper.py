from datetime import date, timedelta, datetime
from dataclasses import dataclass, field


@dataclass
class UseDates(object):
    enddate: date = field(default=date.today().replace(day=1) - timedelta(days=1))
    stdate: date = field(default=(date.today().replace(day=1) - timedelta(days=1)).replace(day=1))
    stdatetime: datetime = field(init=False)
    stdatestr: str = field(init=False)
    enddatetime: datetime = field(init=False)
    enddatestr: str = field(init=False)

    def __post_init__(self):
        self.stdatetime = datetime.combine(self.stdate, datetime.min.time())
        self.enddatetime = datetime.combine(self.enddate, datetime.min.time())
        self.enddatestr = datetime.strftime(self.enddatetime, "%Y-%m-%d")
        self.stdatestr = datetime.strftime(self.stdatetime, "%Y-%m-%d")
