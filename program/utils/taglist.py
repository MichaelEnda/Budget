from dataclasses import dataclass, fields


taglist = [
        ('g', "Gr", 'Groceries'),
        ('a', "Ga", 'Gas'),
        ('e', "E", 'Eatting Out'),
        ('v', "V", 'Vice'),
        ('r', "R", 'Rent'),
        ('b', "B", 'Bill'),
        ('i', "I", 'Investing'),
        ('x', "X", 'Uncategorized Expenditure'),
        ('w', "W", 'Wages'),
        ('s', "S", 'Scholarships'),
        ('o', "OI", 'Other Income'),
        ('p', "P", 'Payback'),
        ('t', "T", 'Transfer')
    ]

@dataclass
class Tag:
    binding: str
    tag: str
    desc: str
    hist_attr: str

@dataclass
class TagList:
    groceries = Tag(
            "g",
            "Gr",
            "Groceries",
            "groceries"
            )
    gas = Tag(
        'a', 
        "Ga", 
        'Gas',
        "gas"
        )
    eatingout = Tag(
            'e', "E", 'Eatting Out', "eatingout"
            )
    vices = Tag(
            'v', "V", 'Vice', 'vices'
            )
    rent = Tag(
            'r', "R", 'Rent', 'rent'
            )
    bill = Tag(
            'b', "B", 'Bill', 'bill'
            )
    investing = Tag(
            'i', "I", 'Investing', 'investing'
            )
    otherexpend = Tag(
            'x',
            "X",
            'Uncategorized Expenditure',
            'otherexpend'
            )
    wages = Tag(
            'w', "W", 'Wages', 'wages'
            )
    scholarships = Tag(
            's', "S", 'Scholarships', 'scholarships'
            )
    otherinc = Tag(
            'o', "OI", 'Other Income', 'otherinc'
            )
    payback = Tag(
            'p', "P", 'Payback', 'payback'
            )
    transfer = Tag(
            't', "T", 'Transfer', 'None'
            )

    @property
    def spending_table_bindings(self):
        binding_list = []
        for field in fields(self):
            tag = getattr(self, field.name)
            print(tag)
            binding_list.append((tag.binding, f'flag_line("{tag.tag}")', tag.desc))
        return binding_list

if __name__ == "__main__":
    print(TagList().spending_table_bindings)
